-- Mission Traitorous Waters

function Initialise()

    SetData("TurnTime", 30000)
    SetData("RoundTime", -1)

    lib_SetupTeam(0, "PlayersTeam")
    lib_SetupWorm(0, "PlayersWorm") 
    lib_SetupTeam(1, "VillageTeam")
    lib_SetupWorm(1, "Villager1") 
    lib_SetupWorm(2, "Villager2") 
    lib_SetupWorm(3, "Villager3") 
    lib_SetupWorm(4, "Villager4") 
    lib_SetupWorm(5, "Villager5") 
    lib_SetupWorm(6, "Villager7") 
    lib_SetupWorm(7, "Villager8") 
    lib_SetupWorm(8, "Villager9") 
    lib_SetupWorm(9, "Villager10") 
    lib_SetupWorm(10, "Villager6")     
    lib_SetupTeam(2, "BoatingBandits")
--~     lib_SetupWorm(10, "Enemy1") 
    --lib_SetupWorm(11, "Enemy2") 
--~     lib_SetupWorm(11, "Enemy3")  
    --lib_SetupWorm(12, "Enemy4") 
    --lib_SetupWorm(12, "Enemy5") 
    --lib_SetupWorm(13, "Enemy6") 
--~     lib_SetupWorm(14, "Enemy7") 
    SendMessage("WormManager.Reinitialise")
    SendMessage("GameLogic.PlaceObjects")

    lib_SetupTeamInventory(0, "PlayerInventory")
    
    lib_SpawnCrate("Crate1") 
    lib_SpawnCrate("Crate2") 
    
    SetData("Trigger.Visibility", 0)
    lib_SpawnTrigger("BoatGunTrigger_One")
    lib_SpawnTrigger("BoatGunTrigger_Onea")
    lib_SpawnTrigger("BoatGunTrigger_Oneb")
    lib_SpawnTrigger("BoatGunTrigger_Two")
    lib_SpawnTrigger("BoatGunTrigger_Twoa")
    lib_SpawnTrigger("BoatGunTrigger_Twob")
    lib_SpawnTrigger("BoatGunTrigger_Three")
    lib_SpawnTrigger("BoatGunTrigger_Threea")
    lib_SpawnTrigger("BoatGunTrigger_Threeb")
    lib_SpawnTrigger("BoatGunTrigger_Four")
    lib_SpawnTrigger("BoatGunTrigger_Foura")
    lib_SpawnTrigger("BoatGunTrigger_Fourb")
    lib_SpawnTrigger("BoatGunTrigger_Five")
    lib_SpawnTrigger("BoatGunTrigger_Fivea")
    lib_SpawnTrigger("BoatGunTrigger_Fiveb")
    lib_SpawnTrigger("BoatGunTrigger_Six")
    lib_SpawnTrigger("BoatGunTrigger_Sixa")
    lib_SpawnTrigger("BoatGunTrigger_Sixb")
    lib_SpawnTrigger("BoatGunTrigger_Seven")
    lib_SpawnTrigger("BoatGunTrigger_Sevena")
    lib_SpawnTrigger("BoatGunTrigger_Sevenb")
    lib_SpawnTrigger("HouseExplosionTrigger_One")
    lib_SpawnTrigger("HouseExplosionTrigger_Two")
    lib_SpawnTrigger("HouseExplosionTrigger_Three")
    lib_SpawnTrigger("HouseExplosionTrigger_Four")
    lib_SpawnTrigger("HouseExplosionTrigger_Five")
    lib_SpawnTrigger("HouseExplosionTrigger_Six")
    lib_SpawnTrigger("HouseExplosionTrigger_Seven")
    lib_SpawnTrigger("HouseExplosionTrigger_Eight")
    lib_SpawnTrigger("HouseExplosionTrigger_Nine")
    
	-- a table (array) of true/false values, one for each gun
    -- which represents whether it has been destroyed or not
	g_bGunDestroyed = {false, false, false, false, false, false, false}
	g_nNumberOfGunsRemaining = 7
    g_nGunFiring = 4 --1
    
	-- and the same for the houses
	g_bHouseDestroyed = {false, false, false, false, false, false, false, false, false}
	g_nNumberOfHousesRemaining = 9
	g_nNextHouseToBlowUp = 0
    Boat_1_Cannon_1_Gone = false
    Boat_1_Cannon_2_Gone = false
    Boat_2_Cannon_1_Gone = false
    Boat_2_Cannon_2_Gone = false
    Boat_3_Cannon_1_Gone = false
    Boat_3_Cannon_2_Gone = false
    Boat_4_Cannon_1_Gone = false
    NotBeenDestroyedYet1 = false
    NotBeenDestroyedYet2 = false 
    NotBeenDestroyedYet3 = false
    NotBeenDestroyedYet4 = false
    Alreadyblowingthesehousesup = false
    AlreadyKilledTheWorm = false
    DontShowTextAgain = false
    g_nSmokeEmitterId = 0
    EmitterHasBeenSDeleted = false
    
    DONTDOTRIGGERCHECKAGAINFORGODSSAKE = false
    ForSomeReasonTurnEndedGetsCalledTwiceWhenEFMVISPlayed = false
    PlayIntroMovie()

end	

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    
    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Boat2Gun1" or WhichMovie == "Boat2Gun2" or WhichMovie == "Boat3Gun1" or WhichMovie == "Boat3Gun2" or WhichMovie == "Boat4Gun1"
                        or WhichMovie == "Boat4Gun2" or WhichMovie == "Boat5Gun1" then
                        
                        
                        
            if Alreadyblowingthesehousesup == false then
                CancelTimer(HouseTimer)
               BlowThemHousesUp() 
               
            end
--            
            if AlreadyKilledTheWorm == false then
                CancelTimer(WormKillTimer)
                SilentKillWormInTheAir() 
                
            end
            
            if EmitterHasBeenSDeleted == false then
    
                if g_nSmokeEmitterId ~= 0 then lib_DeleteEmitter(g_nSmokeEmitterId) end
    
                
    
            end
            
            CancelTimer(GunTimer)
            
            --if g_nSmokeEmitterId ~= 0 then lib_DeleteEmitter(g_nSmokeEmitterId) end
            
            
        if g_nNumberOfHousesRemaining == 0 then
			
            SetData("EFMV.GameOverMovie.Off",1) -- we dont want the fireworks and dont have an outro
            SendMessage("GameLogic.Mission.Failure")
            
        
        
--        
       else

            StartTurn()
            Alreadyblowingthesehousesup = false
            AlreadyKilledTheWorm = false
            
        end
        
        
    elseif WhichMovie == "Intro" then
        
        SendIntMessage("Worm.DieQuietly", 10)
        StartFirstTurn()
    
    end
end


function Trigger_Destroyed()

    TriggerIndex = GetData("Trigger.Index")
    
	if TriggerIndex < 5 then -- This means the houses
		
		-- set the appropriate house as now destroyed
		g_bHouseDestroyed[TriggerIndex] = true
        WormKillTimer = StartTimer("SilentKillWormInTheAir", 3000)
		
		-- if all houses are destroyed then game over
		g_nNumberOfHousesRemaining = g_nNumberOfHousesRemaining - 1
    
        if g_nNumberOfHousesRemaining == 0 then
			
            --SetData("EFMV.GameOverMovie.Off",1) -- we dont want the fireworks and dont have an outro
            --SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment() 
        
        end
        

	else -- (ie. triggers >= 5)             
 		
		-- the guns are triggers 5-11
      	-- set the appropriate gun as now destroyed
		g_bGunDestroyed[TriggerIndex - 4] = true -- the guns are indexed 1-6 not 5-10
        lib_DeleteEmitter(g_nSmokeEmitterId)

		-- If all guns destroyed then mission success
        
        if DONTDOTRIGGERCHECKAGAINFORGODSSAKE == false then -- so if more than one trigger is distoryed this is still only run once
        
            g_bGunDestroyed[TriggerIndex - 4] = true -- the guns are indexed 1-6 not 5-10
            g_nNumberOfGunsRemaining = g_nNumberOfGunsRemaining - 1
            DONTDOTRIGGERCHECKAGAINFORGODSSAKE = true
        end
        

        
        if TriggerIndex == 8 and DontShowTextAgain == false then---------------------- BOAT ONE
        
            StartTimer("Boat3BlowUp",1000) 
            
            if g_bGunDestroyed[g_nGunFiring] == true then
            
                DontShowTextAgain = true
            
                SetData("CommentaryPanel.Delay", 2000) -- 2 seconds
                SetData("CommentaryPanel.Comment", "M.Arab.LEE.N2") -- Thats it! 
                SendMessage("CommentaryPanel.TimedText")
                
            end
            
        elseif TriggerIndex == 6 and DontShowTextAgain == false then
        
            StartTimer("Boat7BlowUp",1000)
            
            if  g_bGunDestroyed[g_nGunFiring] == true then
            
                DontShowTextAgain = true
                
                SetData("CommentaryPanel.Delay", 2000) -- 2 seconds
                SetData("CommentaryPanel.Comment", "M.Arab.LEE.N3") -- Show them bandits whos boss! 
                SendMessage("CommentaryPanel.TimedText")
                
            end
            
        -------------------------------------------- BOAT TWO  
            
        elseif TriggerIndex == 9 and DontShowTextAgain == false then
       
            StartTimer("Boat6BlowUp",1000)
            
            if g_bGunDestroyed[g_nGunFiring] == true then
            
                DontShowTextAgain = true
            
                SetData("CommentaryPanel.Delay", 2000) -- 2 seconds
                SetData("CommentaryPanel.Comment", "M.Arab.LEE.N4") -- Only a few more guns to go!  
                SendMessage("CommentaryPanel.TimedText")
                
            end
           
        elseif TriggerIndex == 5 and DontShowTextAgain == false then
        
           StartTimer("Boat2BlowUp",1000) 
           
           if g_bGunDestroyed[g_nGunFiring] == true then
           
                DontShowTextAgain = true
           
                SetData("CommentaryPanel.Delay", 2000) -- 2 seconds
                SetData("CommentaryPanel.Comment", "M.Arab.LEE.N5") -- Those scallywags don't like that! Keep it up!  
                SendMessage("CommentaryPanel.TimedText")
                
            end
            
        ------------------------------------------------- BOAT THREE
        
        elseif TriggerIndex == 10 and DontShowTextAgain == false then
       
           StartTimer("Boat1BlowUp",1000) 
           
           if g_bGunDestroyed[g_nGunFiring] == true then
           
                DontShowTextAgain = true
           
                SetData("CommentaryPanel.Delay", 2000) -- 2 seconds
                SetData("CommentaryPanel.Comment", "M.Arab.LEE.N6") -- Thats it matey! The more guns sunk the better.  
                SendMessage("CommentaryPanel.TimedText")
                
            end
           
        elseif TriggerIndex == 7 and DontShowTextAgain == false then
        
           StartTimer("Boat5BlowUp",1000) 
           
           if g_bGunDestroyed[g_nGunFiring] == true then
           
                DontShowTextAgain = true
           
                SetData("CommentaryPanel.Delay", 2000) -- 2 seconds
                SetData("CommentaryPanel.Comment", "M.Arab.LEE.N7") -- Your giving them scallys a real good flogging.  
                SendMessage("CommentaryPanel.TimedText")
                
            end
            
            
        --------------------------------------------------- BOAT FOUR ONLY ONE GUN
        
        elseif TriggerIndex == 11 and DontShowTextAgain == false then
        
            if g_bGunDestroyed[g_nGunFiring] == true then
            
                DontShowTextAgain = true
        
                SetData("CommentaryPanel.Delay", 2000) -- 2 seconds
                SetData("CommentaryPanel.Comment", "M.Arab.LEE.N8") -- Plenty of pieces of eight for that shot!  
                SendMessage("CommentaryPanel.TimedText")
                
            end
       
           StartTimer("Boat4BlowUp",1000) 
        
        end
        
	end
end 

function Boat1BlowUp()
       lib_CreateExplosion("Gun5", 80, 0.8, 90, 100, 10)
end

function Boat2BlowUp()
        lib_CreateExplosion("Gun4", 80, 0.8, 90, 100, 10)
end

function Boat3BlowUp()
        lib_CreateExplosion("Gun1", 80, 0.8, 90, 100, 10)
end

function Boat4BlowUp()
        lib_CreateExplosion("Gun7", 80, 0.8, 90, 100, 10)
end

function Boat5BlowUp()
        lib_CreateExplosion("Gun6", 80, 0.8, 90, 100, 10)
end

function Boat6BlowUp()
        lib_CreateExplosion("Gun3", 80, 0.8, 90, 100, 10)
end

function Boat7BlowUp()
        lib_CreateExplosion("Gun2", 80, 0.8, 90, 100, 10)
end


function TurnStarted() 
	-- Each new turn release smoke from the next available gun
    -- The stuff here says in English:
	-- "Look at the next gun, (or gun 1 if we last fired gun 6)"
	-- "If this gun is not detroyed then smoke from it, otherwise look at the next one"

	-- Because we end the game as soon as the last gun is destroyed
	-- then we can assume at least one gun is left
    
    DONTDOTRIGGERCHECKAGAINFORGODSSAKE = false
    DontShowTextAgain = false
    
    if ForSomeReasonTurnEndedGetsCalledTwiceWhenEFMVISPlayed == false then 
        ForSomeReasonTurnEndedGetsCalledTwiceWhenEFMVISPlayed = true
        repeat
        g_nGunFiring = g_nGunFiring + 1
        if g_nGunFiring > 7 then g_nGunFiring = 1 end
        
        until g_bGunDestroyed[g_nGunFiring] == false -- keep looking until this gun is not destroyed

        --if g_nSmokeEmitterId ~= 0 then lib_DeleteEmitter(g_nSmokeEmitterId) end
    
        -- a table with 6 detail object names
        local sGunSites = {"Gun4Smoke", "Gun2Smoke", "Gun6Smoke", "Gun1Smoke", "Gun3Smoke", "Gun5Smoke", "Gun7Smoke"}
        
        local sSpecialTarget = {"Target4", "Target2", "Target6", "Target1", "Target3", "Target5", "Target7"}    --khere, table of the targets
        
        -- We create an smoke emitter at the site indexed by the GunFiring number
        g_nSmokeEmitterId = lib_CreateEmitter("WXPL_CannonFuse", sGunSites[g_nGunFiring])
        lib_SpawnCrate(sSpecialTarget[g_nGunFiring])
        EmitterHasBeenSDeleted = false
    end
end

     
function BlowThemHousesUp() 

    Alreadyblowingthesehousesup = true

	-- Select the next undestroyed house (similar to choose the gun)
	-- Again because we end the game as soon as the last house is destroyed
	-- then we can assume at least one is left
	repeat
	    g_nNextHouseToBlowUp = g_nNextHouseToBlowUp + 1
        if g_nNextHouseToBlowUp > 9 then g_nNextHouseToBlowUp = 1 end
	until g_bHouseDestroyed[g_nNextHouseToBlowUp] == false -- keep looking until this house is not yet destroyed


	-- This wow auto creates a particle effect as well   
	-- Vitally it will also destroy the trigger which might end the game

	local sExplosionSites = {"EX1", "EX2", "EX3", "EX4", "EX5","EX7","EX8","EX9","EX10"} -- choose the location
    lib_CreateExplosion(sExplosionSites[g_nNextHouseToBlowUp], 100, 3, 150, 100, 80)

	lib_ShakeCamera(1000, 0.2)
    HousesBlowingUpText()
end

function TurnEnded()
    SendIntMessage("Crate.Delete", 100) -- khere, always deletes the special target

    if g_nNumberOfHousesRemaining == 0 then
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment() 
    end
        
    if g_nNumberOfGunsRemaining == 0 then
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    else
        ForSomeReasonTurnEndedGetsCalledTwiceWhenEFMVISPlayed = false
        -- If player dies game over
        TeamCount = lib_GetActiveAlliances()
        if TeamCount == 0 then
            SendMessage("GameLogic.Mission.Failure") 
            lib_DisplayFailureComment()
            -- If the gun which fired at the start of the turn is not destroyed then fire it
        elseif g_bGunDestroyed[g_nGunFiring] == false then
            StartTimer("Gunblast", 1000)
            HouseTimer = StartTimer("BlowThemHousesUp", 5000)
            GunTimer = StartTimer("ShakeThemGuns", 2000)
        else
            StartTurn()
        end
    end
end

function Gunblast()
    local CameraToUse = {"Boat3Gun2","Boat4Gun2","Boat2Gun1","Boat4Gun1","Boat3Gun1","Boat2Gun2","Boat5Gun1"}
    SetData("EFMV.MovieName", CameraToUse[g_nGunFiring])
    SendMessage("EFMV.Play")
end

function ShakeThemGuns()
    -- Particle effect to show gun firing
    if EmitterHasBeenSDeleted == false then
        if g_nSmokeEmitterId ~= 0 then lib_DeleteEmitter(g_nSmokeEmitterId) 
            EmitterHasBeenSDeleted = true
        end
    end

    local DetailObjectList = { "Guncamlook4", "Guncamlook2", "Guncamlook6", "Guncamlook1", "Guncamlook3", "Guncamlook5" , "Guncamlook7"}
	SetData("Particle.DetailObject", DetailObjectList[g_nGunFiring])
	SetData("Particle.Name", "WXP_Explosion_Small")
	SendMessage("Particle.NewEmitter")
    lib_ShakeCamera(1000, 0.2)
end
    
--function Towncam()
--    SetData("EFMV.MovieName", "Midtro")
--    SendMessage("EFMV.Play")
--end

function SilentKillWormInTheAir() -- To stop the game camera from showing the flying worms death anims
    AlreadyKilledTheWorm = true
    SendIntMessage("Worm.DieQuietly", g_nNextHouseToBlowUp)
    --TurnStarted() 
end

function SetWind()
    local MaxWind = 0.000170
    local WindSpeed = {0.1,0.2,0.3,0.4} -- Values use for the windspeed
    RandomNumber = lib_GetRandom(1,4)
    SetData("Wind.Speed", WindSpeed[RandomNumber]*MaxWind)     --max wind, value taken from number of Triggers Destroyed.    --max wind, value taken from number of Triggers Destroyed.
    SetData("Wind.Direction", 2.5)
end

function HousesBlowingUpText()
    if g_nNextHouseToBlowUp < 9 then
        local TextToDisplay = { "M.Arab.Traitor.NEW.8", "M.Arab.Traitor.NEW.7", "M.Arab.Traitor.NEW.6", "M.Arab.Traitor.NEW.5", "M.Arab.Traitor.NEW.4", "M.Arab.Traitor.NEW.3" , "M.Arab.Traitor.NEW.2", "M.Arab.Traitor.NEW.1"}
        SetData("CommentaryPanel.Delay", 3000) -- 3 seconds
        SetData("CommentaryPanel.Comment", TextToDisplay[g_nNextHouseToBlowUp]) -- Only (Whatever it is) number of worms left! 
        SendMessage("CommentaryPanel.TimedText")
    end
end
