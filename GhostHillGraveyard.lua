
    
function Initialise()


	SetData("TurnTime", 45000)
	SetData("RoundTime", 3000000)	
    lib_SetupWorm(0, "Player_Worm")
    lib_SetupTeam(0, "Player_Team")
    lib_SetupTeam(1, "CPU_Team")
    lib_SetupTeamInventory(0, "Player_Inventory")
    lib_SetupTeamInventory(1, "CPU_Inventory")
    
    
    WormAILevel = "AIParams.CPU1"
    CopyContainer(WormAILevel, "AIParams.Worm01")
    CopyContainer(WormAILevel, "AIParams.Worm02")
    CopyContainer(WormAILevel, "AIParams.Worm03") 
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm05")
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07")
    CopyContainer(WormAILevel, "AIParams.Worm08") 
    CopyContainer(WormAILevel, "AIParams.Worm09")
    CopyContainer(WormAILevel, "AIParams.Worm10")
    
    SendMessage("WormManager.Reinitialise")
    SetData("Land.Indestructable", 0)
    IndexNumber = 0
    WormsDead = 0
    WormNumber = 1
    StopTheLoop = 0
    EmitterNumber = 1
    DONTPLAYTHELASTCUTMOVIEYOUSILLYLITTLETHING = false
    PlayMovie = false
    lib_SpawnCrate("Crate1")
    lib_SpawnCrate("Health_crate")
    lib_SpawnCrate("Health_crate2")
    lib_SpawnCrate("SheepCrate")
    EmitterPostion = {"Party1","Party2","Party3","Party4","Party5"}
    Emitter = lib_CreateEmitter("WXP_CollectableItem", EmitterPostion[EmitterNumber]) 
    AmountOfGoldSpawned = 0
    MissionShouldEnd = false
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 3000         
    CloseContainer(lock)
    
    PlayIntroMovie()

end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()

    local Movie = GetData("EFMV.MovieName")
    
        if Movie == "Intro" then
            
           StartFirstTurn() 
    
       end
       
       if Movie == "Pointless_cut1" or Movie == "Pointless_cut2"or Movie == "Pointless_cut3"or Movie == "Pointless_cut4" then
       
           StartTurn()
           
        end
    
end

function Crate_Collected() -- Spawn the next crate and enemy worm after each crate is collected.

    temp = GetData("Crate.Index")
    if temp == 99 then	   
	    
	    -- bug fix, this is a weapon crate we want to ignore
	    return
    else
	    
	IndexNumber = temp
    
    end
    
    if IndexNumber == 1 then
    
        
        SetData("EFMV.MovieName", "Midtro")
        SendMessage("EFMV.Play")
        SetData("SameTeamNextTurn", 1)
        -- movie will hold open an activity handle until movie done and then TurnEnded will be called
        EndTurn() 
    end
    
    if IndexNumber == 4 then
        
        MissionShouldEnd = true
        lib_DeleteEmitterImmediate(Emitter)
        CheckIfTheMIssionShouldEnd()
        
    end
    
    if IndexNumber >= 1 and IndexNumber <= 4 then
    
        PlayMovie = true
        EmitterNumber = EmitterNumber + 1
        lib_DeleteEmitterImmediate(Emitter)
        local  ZombieSFXPoint = {"DeadWorm1", "DeadWorm3","DeadWorm5","DeadWorm8","DeadWorm10"}
        local  SpawnExplosionPoint = {"DeadWorm1", "DeadWorm2","DeadWorm3","DeadWorm4","DeadWorm5","DeadWorm6", "DeadWorm7","DeadWorm8","DeadWorm9","DeadWorm10"}
        
        lib_CreateEmitter("WXPS_Zombie",ZombieSFXPoint[EmitterNumber])
        

    
        local  Worm = {"CPU_Worm1", "CPU_Worm2", "CPU_Worm3", "CPU_Worm4","CPU_Worm5","CPU_Worm6","CPU_Worm7","CPU_Worm8","CPU_Worm9","CPU_Worm10"}
        
        repeat 
        
        
            lib_CreateEmitter("WXP_Explosion_Small",SpawnExplosionPoint[WormNumber])
            lib_SetupWorm(WormNumber, Worm[WormNumber]) 
            SendIntMessage("Worm.Respawn", WormNumber)  
            WormNumber = WormNumber + 1
            StopTheLoop = StopTheLoop + 1
            
            
        
        until StopTheLoop == IndexNumber
        
        
        StopTheLoop = 0
        
        
    end
    
end

function Worm_Died()	

    local WormId = GetData("DeadWorm.Id")
    
    if WormId == 0 then  

        SendMessage("GameLogic.Mission.Failure")     -- the players worm died
        lib_DisplayFailureComment()
    else
    
        WormsDead = WormsDead + 1
        
    end
    
end
    


function CheckIfTheMIssionShouldEnd()

    if MissionShouldEnd == true and WormsDead == 10 then
    
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    
    
    else
    
        SetData("CommentaryPanel.Delay", 3000) -- 3 seconds
        SetData("CommentaryPanel.Comment", "M.Wild.GHOST.LEENEW1") -- Make sure you exorcism the rest of those ghost worms!
        SendMessage("CommentaryPanel.TimedText")
    
    --texty texty
    
    end
    

end

function TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            SetData("GameToFrontEndDelayTime",1000)
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        else

    
            if MissionShouldEnd == true and WormsDead == 10 then
    
                SetData("EFMV.GameOverMovie", "Outro")
                SendMessage("GameLogic.Mission.Success") 
                lib_DisplaySuccessComment()
            else
            
            
                    Movie = {"Pointless_cut1", "Pointless_cut2", "Pointless_cut3", "Pointless_cut4"}
    
                if DONTPLAYTHELASTCUTMOVIEYOUSILLYLITTLETHING == false then 
    
                    if PlayMovie == true then
    
                        StartTimer("SpawnTheGOLD",1000) 
                        SetData("EFMV.MovieName", Movie[IndexNumber])
                        SendMessage("EFMV.Play")
                        PlayMovie = false
        
                    elseif PlayMovie == false then
                
                        StartTurn()
        
                    end
                    
                elseif DONTPLAYTHELASTCUTMOVIEYOUSILLYLITTLETHING == true then
                
                
    
                StartTurn()
                
                end
            
            end
            
        end
        
end


function SpawnTheGOLD()
    
    AmountOfGoldSpawned = AmountOfGoldSpawned + 1
    
    if AmountOfGoldSpawned < 4 then

        Emitter = lib_CreateEmitter("WXP_CollectableItem", EmitterPostion[EmitterNumber]) 
    
        local  Crate = {"Crate2", "Crate3", "Crate4", "Crate5"}
       
        lib_SpawnCrate(Crate[IndexNumber])
        
    end
        
    if AmountOfGoldSpawned == 3 then
    
        DONTPLAYTHELASTCUTMOVIEYOUSILLYLITTLETHING = true
        
    end
        
end
    
