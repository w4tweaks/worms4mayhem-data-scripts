
-- Tutorial 2
--
-- For me to do:
--      * Player could do with prompting after inactivity..?

function Initialise()
    kVariables()                    -- Lets have a butchers at the variables we're gonna use before the deathmatch begins.    
    kDialogueBoxTimeDelay = 0500    -- This is the delay between selecting a weapon and having the game display a dialogue box.

    SendMessage("Commentary.NoDefault")

    SetData("Mine.DetonationType", 1)
	SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 5000)
	SetData("Mine.MaxFuse", 5000)
    SendMessage("GameLogic.PlaceObjects")

	SetData("HotSeatTime", 0) 
	SetData("RoundTime", -1)
	SetData("TurnTime", 90000)

    lib_SetupTeam(0, "Team_Human")
    lib_SetupTeam(1, "Team_Enemy")
    lib_SetupTeam(2, "Team_EFMV")
    
    lib_SetupWorm(0, "Worm.Game1.Human0")
    lib_SetupWorm(1, "Worm.Game1.Human1")
    lib_SetupWorm(2, "Worm.Game1.Human2")
    lib_SetupWorm(3, "Worm.Game1.Human3")

    lib_SetupTeamInventory(0, "Inventory_Human")
    lib_SetupTeamInventory(1, "Inventory_Enemy")

    SendMessage("WormManager.Reinitialise")

    SetData("Trigger.Visibility",0)

    SetData("Land.Indestructable",1)

    SetData("Wind.Speed", 0)

    lock, scheme = EditContainer("GM.SchemeData")
    scheme.HelpPanelDelay = 0          
    CloseContainer(lock)
	
    SetData("EFMV.Unskipable", 0)
    -- Kick the game off by running the first little EFMV snippet.
    kPlayEFMV("EFMV.Intro")
end

function EFMV_Terminated()
	local WhichMovie = GetData("EFMV.MovieName")
    if WhichMovie == "EFMV.Intro" then
        CopyContainer("AIParams.CPU1", "AIParams.Worm04")
        CopyContainer("AIParams.CPU1", "AIParams.Worm05")
        CopyContainer("AIParams.CPU1", "AIParams.Worm06")
        CopyContainer("AIParams.CPU1", "AIParams.Worm07")
        CopyContainer("AIParams.CPU1", "AIParams.Worm08")
    
        lib_SetupWorm(15, "Actor.Worm15.Worminkle")
        SendIntMessage("Worm.Respawn", 15)
    
        Create_Game1_Wave1_Crates()
        Create_UtilityCrates()
        --Create_Upperplatform_Triggers()
        
        SetData("EFMV.Unskipable", 1)
        kPlayEFMV("EFMV.Intro_Dialogue")
    elseif WhichMovie == "EFMV.Intro_Dialogue" then
        SetData("EFMV.Unskipable", 0)
        SetUpWormsAfter_EFMV_Intro()
    elseif WhichMovie == "EFMV.Game1.Crates1_LI" then
        kPlayEFMV("EFMV.Game1.Crates1_End")
    elseif WhichMovie == "EFMV.Game1.Crates2_LI" then
        kPlayEFMV("EFMV.Game1.Crates2_End")
    elseif WhichMovie == "EFMV.Outtro" then
        kSendSuccessMessage()
    elseif WhichMovie == "EFMV.Failure" then
        SetData("EFMV.Unskipable", 0)
        SendIntMessage("WXWormManager.UnspawnWorm", 14)
        StartTurn()
    elseif WhichMovie == "EFMV.Game2.NinjaRope" then
        g_BoxId = "null"
		SetData("WeaponPanel.AllowOpen", 1 )
    end
end

function SetUpWormsAfter_EFMV_Intro()
    kWhereAreWe = 10    -- Change the kWhereAreWe variable to 10 so this doesn't affect the worm died function.
    SendIntMessage("WXWormManager.UnspawnWorm", 15)
    kWhereAreWe = 0     -- Now we change it back!

    lock, team_data = EditContainer("Team.Data00")
    team_data.GraveIndex = 255
    CloseContainer(lock)

    StartFirstTurn()
end

--~ function Game_BriefingDialogNowOff()
--~     if g_BoxId == "T2_R.NinjaRopeSelected" then
--~         g_BoxId = "null"
--~         kPlayEFMV("EFMV.Game2.NinjaRope")
--~     end
--~ end

function TurnStarted()
    kTurnCounter = kTurnCounter + 1
    
    SetData("Wind.Speed", 0)

	if kNinjaRopeHasBeenUsed == 2 then
		kNinjaRopeHasBeenUsed = 3
		SetData("HotSeatTime", 3000)
	end
	
    if kTurnCounter == 1 then
        kPlayEFMV("PositionCam")
        StartTimer("Delay_HUD_Anim", 0350)
        StartTimer("Delay_FirstDialogueBox", 1100)
    elseif kTurnCounter == 2 then
        StartTimer("Delay_TurnPassingOverDialogueBox", 1000)
		SetData("HotSeatTime", 3000)
    end
end

function Delay_FirstDialogueBox()
    kMakeBriefingBox("T2_R.Opening1")
end

function Delay_HUD_Anim()
    kPlayEFMV("Hud.TurnTime")
end

function Delay_TurnPassingOverDialogueBox()
    kMakeBriefingBox("T2_R.TurnEnd")
end

function Delay_SecondHumanTurnDialogueBox()
    kMakeBriefingBox("T2_R.TurnStarts")
end

function TurnEnded()
    if kNinjaRopeHasBeenUsed == 1 then
        kNinjaRopeHasBeenUsed = 2
        kMakeBriefingBox("T2_R.NinjaRopeExtra")
    end

    if kEnemyWormsAlive == 0 then
        kDontShowFailureSequence = true
        StartTimer("kTutorialOutro", 0500)
    elseif kHumanWormsAlive == 0 and kDontShowFailureSequence == false then
        --kPlayFailureEFMV = true
        lib_SetupWorm(0, "Worm.Game1.Human0")
        lib_SetupWorm(1, "Worm.Game1.Human1")
        lib_SetupWorm(2, "Worm.Game1.Human2")
        lib_SetupWorm(3, "Worm.Game1.Human3")
        
        SendIntMessage("Worm.Respawn", 0)
        SendIntMessage("Worm.Respawn", 1)
        SendIntMessage("Worm.Respawn", 2)
        SendIntMessage("Worm.Respawn", 3)
        
        kHumanWormsAlive = 4
        
        lib_SetupWorm(14, "Actor.Worm14.Worminkle2")
        SendIntMessage("Worm.Respawn", 14)
        
        SetData("EFMV.Unskipable", 1)
        kPlayEFMV("EFMV.Failure")
    else
        StartTurn()
    end
end
function Worm_Died()	
	deadworm = GetData("DeadWorm.Id")
    
    if deadworm == 0
    or deadworm == 1
    or deadworm == 2
    or deadworm == 3 then
        kHumanWormsAlive = kHumanWormsAlive -1
    elseif deadworm == 4
    or deadworm == 5
    or deadworm == 6
    or deadworm == 7
    or deadworm == 8 then
        kEnemyWormsAlive = kEnemyWormsAlive -1
        
        if kEnemyWormsAlive == 4 then
            StartTimer("Delay_FirstWormDiedDBox", 0700)
        elseif kEnemyWormsAlive == 1 then
            StartTimer("Delay_FourthWormDiedDBox", 0700)
        end
    end
end

function Delay_FirstWormDiedDBox()
    kMakeBriefingBox("T2_R.FirstEnemyWormDead")
end

function Delay_FourthWormDiedDBox()
    kMakeBriefingBox("T2.Game1.ThirdEnemyWormDead")
end

function Weapon_Selected()
	local worm = lib_QueryWormContainer()
    
    kWormTeamIndex = worm.TeamIndex
    kWormWeaponIndex = worm.WeaponIndex
   
    if worm.TeamIndex == 1 then return end
	
	if worm.WeaponIndex == "UtilityNinjaRope" and kNinjaRopeSelected == false then
        kNinjaRopeSelected = true
        kNinjaRopeHasBeenUsed = 1
		SetData("HotSeatTime", 0)
        StartTimer("NinjaRope_DBox", kDialogueBoxTimeDelay)
	end
--~     if worm.WeaponIndex == "WeaponDynamite" and kDynamiteSelected == false then
--~         kDynamiteSelected = true
--~         StartTimer("Dynamite_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "WeaponClusterGrenade" and kClusterBombSelected == false then
--~         kClusterBombSelected = true
--~         StartTimer("ClusterBomb_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "WeaponAirstrike" and kAirstrikeSelected == false then
--~         kAirstrikeSelected = true
--~         StartTimer("Airstrike_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "WeaponHomingMissile" and kHomingMissileSelected == false then
--~         kHomingMissileSelected = true
--~         StartTimer("HomingMissile_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "WeaponBaseballBat" and kBaseballBatSelected == false then
--~         kBaseballBatSelected = true
--~         StartTimer("BaseballBat_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "UtilityParachute" and kParachuteSelected == false then
--~         kParachuteSelected = true
--~         StartTimer("Parachute_DBox", kDialogueBoxTimeDelay)
--    elseif worm.WeaponIndex == "UtilityGirder" and kGirderSelected == false then
--        kGirderSelected = true
--        StartTimer("Girder_DBox", kDialogueBoxTimeDelay)
--~     end
end

function Dynamite_DBox()
    kMakeBriefingBox("T2_R.Dynamite")
end

function ClusterBomb_DBox()
    kMakeBriefingBox("T2_R.Cluster")
end

function Airstrike_DBox()
    kMakeBriefingBox("T2_R.Strike")
end

function HomingMissile_DBox()
    kMakeBriefingBox("T2_R.HomingMiss")
end

function NinjaRope_DBox()
    SetData("WeaponPanel.AllowOpen", 0 )
--~     kMakeBriefingBox("T2_R.NinjaRopeSelected")
	g_BoxId = "null"
    kPlayEFMV("EFMV.Game2.NinjaRope")
end

function BaseballBat_DBox()
    kMakeBriefingBox("T2_R.Baseball")
end

function Parachute_DBox()
    kMakeBriefingBox("T2_R.Parachute")
end

function Crate_Collected()
	CrateNumber = GetData("Crate.Index")

    if CrateNumber == 1 then
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
        lib_Comment("Text.kWeaponBaseballBat")
    elseif CrateNumber == 2 then
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
        lib_Comment("Text.kWeaponBaseballBat")
        kDeleteTrigger(1)
    elseif CrateNumber == 3 then
        lib_Comment("Text.kUtilityParachute")
        kDeleteTrigger(1)
    elseif CrateNumber == 4 then
        lib_Comment("Text.kUtilityNinjaRope")
    elseif CrateNumber == 10 then
        lib_Comment("Text.kWeaponAirstrike")
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
    elseif CrateNumber == 20 then
        lib_Comment("Text.kWeaponClusterGrenade")
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
    elseif CrateNumber == 30 then
        lib_Comment("Text.kWeaponDynamite")
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
    elseif CrateNumber == 40 then
        lib_Comment("Text.kWeaponHomingMissile")
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
    end

    if kFirstCrateCollected == false then
        kFirstCrateCollected = true
        kMakeBriefingBox("T2_R.WPRemind")
    end
end

function Trigger_Collected()
	TriggerIndex = GetData("Trigger.Index")
    
	if TriggerIndex == 1 then
        kDeleteTrigger(1)
        StartTimer("Delay_EFMV_CollectThese", 0500)
    end
end

function Delay_EFMV_CollectThese()
    kPlayEFMV("EFMV.CollectThese")
end

-- ***************************************************
-- **                VARIABLE ROOM                  **
-- ***************************************************

function kVariables()
    kNextBitOfText = "notextyet"

    kFirstCrateCollected = false
    kNumberOfCratesCollected = 0
    kNextCrateCycle = 2
    
    kDynamiteSelected = false
    kClusterBombSelected = false
    kAirstrikeSelected = false
    kHomingMissileSelected = false
    kNinjaRopeSelected = false
    kBaseballBatSelected = false
    kParachuteSelected = false
    
    kFirstTurnEndedMessageDisplayed = false
    
    kEnemyWormsAlive = 5
    kHumanWormsAlive = 4
    
    kThisIsFirstTurn = true
    
    kTurnCounter = 0
    
    kDontShowFailureSequence = false
    
    kPlayFailureEFMV = false
    
    kNinjaRopeHasBeenUsed = 0
end

-- ***************************************************
-- **                 ENGINE ROOM                   **
-- ***************************************************

function kDeleteTrigger(kWhichTrigger)
    SendIntMessage("GameLogic.DestroyTrigger",kWhichTrigger)
end

function kMakeBriefingBox(kSayWhat)
    g_BoxId = kSayWhat
    --lib_CreateBriefingBox(g_BoxId)
    lib_CreateWXBriefingBox("WXFEP.InGameBriefMiddle",g_BoxId,"FE.FramedGfx:n")
end

function kPlayEFMV(kWhatIsEFMVCalled)
    SetData("EFMV.MovieName", kWhatIsEFMVCalled)
    SendMessage("EFMV.Play") 
end

function kTutorialOutro()
    lib_SetupWorm(14, "Actor.Worm14.Worminkle2")
    SendIntMessage("Worm.Respawn", 14)
    g_BoxId = "null"
    
    SetData("EFMV.Unskipable", 1)
    SetData("EFMV.GameOverMovie", "EFMV.Outtro")
    kSendSuccessMessage()
end

function kSendSuccessMessage()
    SendMessage("GameLogic.Mission.Success")
end

-- ***************************************************
-- **                    CRATES                     **
-- ***************************************************

function Create_Game1_Wave1_Crates()
    lib_SpawnCrate("WeaponCrate.Game1.Airstrike")
    lib_SpawnCrate("WeaponCrate.Game1.ClusterBomb")
    lib_SpawnCrate("WeaponCrate.Game1.Dynamite")
    lib_SpawnCrate("WeaponCrate.Game1.HomingMissile")
    lib_SpawnCrate("WeaponCrate.Game2.BaseballBat1")
end

function Create_Game1_Wave2_Crates()
    lib_SpawnCrate("WeaponCrate.Game1.Airstrike_2")
    lib_SpawnCrate("WeaponCrate.Game1.ClusterBomb_2")
    lib_SpawnCrate("WeaponCrate.Game1.Dynamite_2")
    lib_SpawnCrate("WeaponCrate.Game1.HomingMissile_2")
    lib_SpawnCrate("WeaponCrate.Game2.BaseballBat1_2")
end

function Create_UtilityCrates()
    lib_SpawnCrate("UtilityCrate.Game2.NinjaRopeXInf")
    lib_SpawnCrate("UtilityCrate.Game2.Parachute1")
end

-- ***************************************************
-- **                 TRIGGER ROOM                  **
-- ***************************************************

--function Create_Upperplatform_Triggers()
--    lib_SpawnTrigger("Trig.UpperPlat1")
--    lib_SpawnTrigger("Trig.UpperPlat2")
--    lib_SpawnTrigger("Trig.UpperPlat3")
--    lib_SpawnTrigger("Trig.UpperPlat4")
--    lib_SpawnTrigger("Trig.UpperPlat5")
--    lib_SpawnTrigger("Trig.UpperPlat6")
--    lib_SpawnTrigger("Trig.UpperPlat7")
--    lib_SpawnTrigger("Trig.UpperPlat8")
--    lib_SpawnTrigger("Trig.UpperPlat9")
--    lib_SpawnTrigger("Trig.UpperPlat10")
--    lib_SpawnTrigger("Trig.UpperPlat11")
--    lib_SpawnTrigger("Trig.UpperPlat12")
--    lib_SpawnTrigger("Trig.UpperPlat13")
--    lib_SpawnTrigger("Trig.UpperPlat14")
--end
