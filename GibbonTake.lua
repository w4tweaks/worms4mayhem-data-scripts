
-- GibbonTake

-- Jetpack will be replaced with Redbull once its working..

function Initialise()

    SetData("TurnTime", 0)
    SetData("RoundTime", 900000)
    
    ---	Worms and teams setup here
    lib_SetupTeam(0, "Player_Team")
    lib_SetupTeam(1, "Ali_Baboons_Team")

	lib_SetupWorm(0, "Player_Worm")  -- Players worm
    lib_SetupWorm(1, "AliBaboon_Worm")
    SendMessage("WormManager.Reinitialise")

	lib_SetupTeamInventory(0, "Player_Inventory")
    lib_SetupTeamInventory(1, "Ali_Inventory")


    lib_SpawnCrate("Gem1") 
    Emitter1 = lib_CreateEmitter("WXP_TreasureTwinkle", "Twinkle1")   
    	
    SetData("Mine.DudProbability", 0)
    SetData("Mine.DetonationType", 1) -- Normal mine explosion
    
    
    SendStringMessage("GameLogic.PlaceMine", "mine1")
    Mine1Id = GetData("Mine.Id")
    
    SendStringMessage("GameLogic.PlaceMine", "mine2")
    Mine2Id = GetData("Mine.Id")
    
    SetData("Trigger.Visibility", 0)
    
    lib_SpawnTrigger("TriggerGate1")
    lib_SpawnTrigger("TriggerGate2")

--~     lib_SpawnTrigger("Trigger1")
    
    SendMessage("GameLogic.PlaceObjects")

    Gate1Destroyed = false
    Gate2Destroyed = false
    WaterRate = 0
    WaterAddition = 0
    Switch = 0
    WaterRising = true
    water = 0
    CrateToSpawn = 0
    water = 0
    WaterStage = 1
    PlayIntroMovie()
    
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "GibbonTakeIntro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
--~     WaterRise()
    StartFirstTurn()
end

function Crate_Collected()

    lib_DeleteEmitterImmediate(Emitter1)
--~     SendMessage("Commentary.NoDefault")
    CrateToSpawn = CrateToSpawn + 1
    
    if CrateToSpawn < 19 then
    
        local CrateLocation = {"Gem2", "Gem3", "Gem4", "Gem5", "Gem6",
                                "NinjaCrate1", "NinjaCrate2", "Gem7", "Gem8", "Scouser_Crate", 
                                "Gem9", "Parachute_Crate", "Gem10", "NinjaCrate3", "Gem11", "Gem12", "Girder_Crate", "Gem13"}
                                
        local EffectLocation = { "Twinkle2", "Twinkle3", "Twinkle4", "Twinkle5", "Twinkle6",
                                "NinjaRopeCrateTwinkle", "NinjaRopeCrate2Twinkle", "Twinkle7", "Twinkle8", "ScouserCrateTwinkle", 
                                "Twinkle9", "ParachuteCrateTwinkle", "Twinkle10", "NinjaRopeCrate3Twinkle", "Twinkle11", "Twinkle12", "GirderCrateTwinkle", "Twinkle13"}
    
        lib_SpawnCrate(CrateLocation[CrateToSpawn]) 
        Emitter1 = lib_CreateEmitter("WXP_TreasureTwinkle", EffectLocation[CrateToSpawn])  
    end
    
    if  CrateToSpawn == 8 then

--~         SetData("CommentaryPanel.Delay", 4000) -- 3 seconds
        SetData("CommentaryPanel.Comment", "M.Arab.Gibbon.1") --Lets see if you will find the next crate as easy!  
        SendMessage("CommentaryPanel.TimedText")


        lock, inventory = EditContainer("Inventory.Alliance00") -- To stop cheating!!!
        inventory.NinjaRope = 0
        CloseContainer(lock)

        SendMessage("Commentary.Clear")
    
    elseif CrateToSpawn == 10 then
    
--~         SetData("CommentaryPanel.Delay", 4000) -- 3 seconds
--~         SetData("CommentaryPanel.Comment", "M.Arab.Gibbon.2") -- Best rise that water a bit more..
--~         SendMessage("CommentaryPanel.TimedText")
--~     
--~         WaterStage = 2
--~         SetData("Jetpack.InitFuel", 5500)
            
    elseif CrateToSpawn == 13 then

--~         SetData("CommentaryPanel.Delay", 4000) -- 3 seconds
        SetData("CommentaryPanel.Comment", "M.Arab.Gibbon.6" ) -- You will never do my course, its far too fiendish for the likes of you!
        SendMessage("CommentaryPanel.TimedText")
    
        lock, inventory = EditContainer("Inventory.Alliance00") -- To stop cheating!!!
        inventory.Jetpack = 0
        CloseContainer(lock)
        
        SendMessage("Commentary.Clear")

    elseif CrateToSpawn == 14 then
    
--~         SetData("CommentaryPanel.Delay", 4000) -- 3 seconds
        SetData("CommentaryPanel.Comment", "M.Arab.Gibbon.3" ) -- My splendit gates of gold will keep you out..
        SendMessage("CommentaryPanel.TimedText")
    
--~         WaterStage = 3

    elseif CrateToSpawn == 15 then
    
        lock, inventory = EditContainer("Inventory.Alliance00") -- To stop cheating!!!
        inventory.Redbull = 0
        CloseContainer(lock)
            
    elseif CrateToSpawn == 18 then
    
--~         WaterStage = 4
            
        lock, inventory = EditContainer("Inventory.Alliance00")
        inventory.NinjaRope = 0
        CloseContainer(lock)
        
        SendMessage("Commentary.Clear")
        
        lib_SpawnTrigger("Trigger1") 
        
    end

    if CrateToSpawn < 7 or CrateToSpawn == 8 or CrateToSpawn == 9 or CrateToSpawn == 11 or CrateToSpawn == 13 or CrateToSpawn == 15 or CrateSpawn == 16 then 
        local Comment = { "C.Generic.Good.1", "C.Generic.Good.2", "C.Generic.Good.3" }
        local MyRand = lib_GetRandom(1,3)
        SendMessage("Commentary.Clear")  
        SetData("CommentaryPanel.Comment", Comment[MyRand] )
        SetData("CommentaryPanel.Delay", 3000)
        SendMessage("CommentaryPanel.TimedText")
    end
    
end


function Payload_Deleted() -- Check the mines, if gone then spawn in some more.

StartTimer("CheckMines",10000)


end


function CheckMines()

  PayloadId = GetData("Payload.Deleted.Id")
  
  if PayloadId == Mine1Id and Gate1Destroyed == false then
  
      SendStringMessage("GameLogic.PlaceMine", "mine1")
      Mine1Id = GetData("Mine.Id")
   
   elseif PayloadId == Mine2Id and Gate2Destroyed == false then
   
       SendStringMessage("GameLogic.PlaceMine", "mine2")
       Mine2Id = GetData("Mine.Id")
   
  end
  
  
end


function Trigger_Collected()

	SetData("Water.Level", 1)
	WaterStage = 5

    lib_SpawnCrate("OutroGem1") 
    lib_CreateEmitter("WXP_TreasureTwinkle", "TreasureTwinkle")          
    
    StartTimer("DoOutroStuff", 1000)
    
end

function DoOutroStuff()
    
    lib_SetupWorm(2, "Dummy1") 
    SendIntMessage("Worm.Respawn", 2)  
   
    lib_SetupWorm(3, "Dummy2") 
    SendIntMessage("Worm.Respawn", 3)  
   
    lib_SetupWorm(4, "Dummy3") 
    SendIntMessage("Worm.Respawn", 4)  
    
   
    SetData("EFMV.GameOverMovie", "Outro")
    SetData("WXD.StoryMovie", "Jurassic")
    SendMessage("GameLogic.Mission.Success") -- Player has reached Ali Baboon
end

function Worm_Died()

    local WormId = GetData("DeadWorm.Id")
    
    if WormId == 1 then
        
    -- CPU has died so some text here
        SendMessage("GameLogic.Mission.Failure")
        --lib_Comment("M.Arab.Gibbon.5" ) -- Killed by my own course
        
        SetData("CommentaryPanel.Delay", 4000) -- 3 seconds
        SetData("CommentaryPanel.Comment", "M.Arab.Gibbon.5" )-- Killed by my own course
        SendMessage("CommentaryPanel.TimedText")
    
    elseif WormId == 0 then

        SetData("EFMV.GameOverMovie", "OutroFail")
        SendMessage("GameLogic.Mission.Failure")
        --lib_Comment("M.Arab.Gibbon.4" ) -- HA!, you will never be good enougth to complete my fiendish course!!
        
--~         SetData("CommentaryPanel.Delay", 4000) -- 3 seconds
--~         SetData("CommentaryPanel.Comment", "M.Arab.Gibbon.4" )-- HA!, you will never be good enougth to complete my fiendish course!!
--~         SendMessage("CommentaryPanel.TimedText")
    
    end
    
end

--~ function WaterRise() -- Yes I stole this code from To boldly go but time is money people!!!

--~     WaterRiseTimer = StartTimer("rise", 250)
--~     
--~ end

--~ function rise() 

--~     if WaterStage == 1 then
--~         
--~         water = water + 0.3
--~         SetData("Water.Level", water)
--~         WaterRise()
--~             
--~     elseif WaterStage == 2 then
--~     
--~         water = water + 0.6
--~         SetData("Water.Level", water)
--~         WaterRise()
--~             
--~     elseif WaterStage == 3 then
--~     
--~         water = water + 1
--~         SetData("Water.Level", water)
--~         WaterRise()
--~             
--~     elseif WaterStage == 4 then
--~     
--~         water = water + 2
--~         SetData("Water.Level", water)
--~         WaterRise()
--~     end
--~ end

function Crate_Sunk() -- Just in case

    
    SendMessage("GameLogic.Mission.Failure")
    lib_DisplayFailureComment()

end


function TurnEnded() -- To stop the mission ending when the girders is used

    -- if the time expires then the mission is failed
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
    
    if RoundTimeRemaining <= 0 then
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    else    
    
        StartTurn()
        
    end
        
end


function Timer_GameTimedOut()

    --SetData("GameToFrontEndDelayTime",1000)
    
    SendMessage("GameLogic.Mission.Failure")
    lib_DisplayFailureComment()

end


function Trigger_Destroyed()

    TriggerIndex = GetData("Trigger.Index")


    if TriggerIndex == 2 then -- gate 1 gone
    
        Gate1Destroyed = true

    elseif TriggerIndex == 3 then
    
        Gate2Destroyed = true
        
    end
    
end



