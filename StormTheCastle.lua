-- Here we go, re-writing scripts a few weeks before release is always good fun!!!

function Initialise()

    SetData("TurnTime", 60000)
    SetData("RoundTime", 3000000)

    -- Create the trigger and set it visible while debugging
  	 SetData("Trigger.Visibility", 0)
    lib_SpawnTrigger("Trigger1")
    lib_SpawnTrigger("Trigger2")
    lib_SpawnTrigger("Trigger3")
    lib_SpawnTrigger("Trigger4")
    SetData("Land.Indestructable", 1)

    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 2000         
    CloseContainer(lock)

    -- These details are copied from the "databank" for this mission
    -- The players "Mission Team" name and so on will also be TIC
    lib_SetupTeam(0, "Human Team")
    lib_SetupWorm(0, "Human Worm1")
    lib_SetupWorm(1, "Human Worm2")
    lib_SetupWorm(2, "Human Worm3")
    lib_SetupWorm(3, "Human Worm4")

    lib_SetupTeam(1, "Royal Team")
    lib_SetupWorm(5, "Royal Archer1")
    lib_SetupWorm(6, "Royal Archer2")
    lib_SetupWorm(7, "Royal Archer3")
    lib_SetupWorm(8, "Royal Archer4")
    --lib_SetupWorm(9, "The King")

    WormAILevel = "AIParams.CPU1"
    CopyContainer(WormAILevel, "AIParams.Worm05")
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07")
    CopyContainer(WormAILevel, "AIParams.Worm08")
 
    CopyContainer(WormAILevel, "AIParams.Worm10")
    CopyContainer(WormAILevel, "AIParams.Worm11")
    CopyContainer(WormAILevel, "AIParams.Worm12")
    CopyContainer(WormAILevel, "AIParams.Worm13")
    CopyContainer(WormAILevel, "AIParams.Worm14")
    
    -- No they are all setup, spawn them all in together
    SendMessage("WormManager.Reinitialise")

    -- Again cloned from the one in the databank
    lib_SetupTeamInventory(0, "Human Inventory")
    lib_SetupTeamInventory(1, "Royal Inventory")

    lib_SpawnCrate("Health_Crate1")
    lib_SpawnCrate("Health_Crate2")
    lib_SpawnCrate("Health_Crate3")
    lib_SpawnCrate("Health4")
    lib_SpawnCrate("GirderCrate")
    lib_SpawnCrate("TimeMachineCrate")
    
    PlayIntroMovie()
    EnemyWormsDead = 0
    PlayerWormsDead = 0
    SpawnACrate = 0
    CrateHasBeenCollected = false
    TimeMachinePartsHaveBeenCollected = false
    StartTheReminderCount = false
    TriggerCollected = false
    Reminder = 0
    IndexNumber = 0
    DontPlayAgain = false
    PlayTheMovie = false
    ClipAlreadyPlayed = false
    AllOver = false
--~     CrateToSpawn = {"Health_Crate1","Health_Crate2","Health_Crate3","Health4"}
    Crate2ToSpawn = {"Poison_Crate1","Poison_Crate2","Poison_Crate3","Poison_Crate4"}
    
    Emitter1 = lib_CreateEmitter("WXP_TimePieces", "TimePieces") 

end


function PlayIntroMovie()

    SetData("EFMV.MovieName", "StormTheCastleIntro")
    SendMessage("EFMV.Play")
    
end

function Crate_Collected() 

    IndexNumber = GetData("Crate.Index")
    
    if IndexNumber == 1 or IndexNumber == 2 or IndexNumber == 3 or IndexNumber == 4 then -- Health crates
    
        CrateHasBeenCollected = true
        
    end
    
    if IndexNumber == 10 then
    
        lib_DeleteEmitterImmediate(Emitter1)
    
        if EnemyWormsDead == 8 then
        
            endmissionsuccess()
        
        end
    
        TimeMachinePartsHaveBeenCollected = true
        
    end
    
    if IndexNumber == 6 then
    
        PlayTheMovie = true
        
    end
        
end

function Crate_Destroyed()

    IndexNumber = GetData("Crate.Index")
    
    if IndexNumber == 10 then
    
        endmissionfail()
        lib_DeleteEmitterImmediate(Emitter1)
        
    end
    
end


function Worm_Died()

    local WormId = GetData("DeadWorm.Id")
    
    if WormId > 4 then  -- one of the enemy worms has died.
    
        EnemyWormsDead = EnemyWormsDead + 1
        
    elseif WormId < 5 then -- One of the players worms have died.
    
        PlayerWormsDead = PlayerWormsDead + 1
    
    end
    
    if WormId == 5 or WormId == 6 or WormId == 7 or WormId == 8 then
    
            WormId = WormId - 4
    
            lib_SpawnCrate(Crate2ToSpawn[WormId])
            
    end
    
end

function TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then -- Round time has run out.
         
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        else -- if round time ok

            TeamCount = lib_GetActiveAlliances()
            WhichTeam = lib_GetSurvivingTeamIndex()
    
            SendIntMessage("GameLogic.DestroyTrigger", 2) -- Respawn Time Machine bit 
            lib_SpawnTrigger("Trigger2")
    
	
            if TeamCount == 0 then
    
                endmissionfail()
                
                AllOver = true
        
            elseif TeamCount == 1 then
    
                if PlayerWormsDead == 4 then
        
                    endmissionfail()
                    AllOver = true
                    
                end
        
            end
            
            
                
        if EnemyWormsDead == 8 and TimeMachinePartsHaveBeenCollected == true then
    
            endmissionsuccess()
        
        else
        
        
        if AllOver == false then
    
            if CrateHasBeenCollected == true then
    
                SpawnACrate = SpawnACrate + 1
                
            end
        
--~                 if SpawnACrate == 4 then
--~                 
--~                 local myRandomInteger = lib_GetRandom(1, 4)
--~         
--~                     lib_SpawnCrate(CrateToSpawn[myRandomInteger])
--~             
--~                     SpawnACrate = 0
--~                     CrateHasBeenCollected = false
                    
                    
--~                 end
                
            StartTurn()

        end
        
        end
        
    end

end

function DoOncePerTurnFunctions()

    if StartTheReminderCount == true then
            
            
        Reminder = Reminder + 1
                
        if Reminder == 2 and TriggerCollected == false then
                
            SetData("EFMV.MovieName", "UseGirderClip")
            SendMessage("EFMV.Play")
                    
            TriggerCollected = true
                    
        end
           
        
    end
    
    
    if EnemyWormsDead == 8 and TimeMachinePartsHaveBeenCollected == false then    
        if ClipAlreadyPlayed == false then
            SetData("EFMV.MovieName", "CollectPart")
            SendMessage("EFMV.Play")
            ClipAlreadyPlayed = true
        end
    end
    
    
    if PlayTheMovie == true then -- Girder crate has been collected.
    
        if DontPlayAgain == false then
    
            StartTimer("SpawnWizardBack",1000) -- I had to move this movie into here as having it on crate
                                        -- collection broke the game in a nasty way.
            lib_SetupWorm(10, "Royal Knight1")
            lib_SetupWorm(11, "Royal Knight2")
            lib_SetupWorm(12, "Royal Knight3")
--~             lib_SetupWorm(13, "Royal Knight4")

            SendIntMessage("Worm.Respawn", 10) 
            SendIntMessage("Worm.Respawn", 11)
            SendIntMessage("Worm.Respawn", 12) 
--~             SendIntMessage("Worm.Respawn", 13)
            WormSpawnMovieNotAlreadyPlayed = true
            SetData("EFMV.MovieName", "EnemySpawnCuts")
            SendMessage("EFMV.Play")
        
        
            StartTheReminderCount = true
            
            DontPlayAgain = true
            
        end
    end
    
end



function EFMV_Terminated()

   local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "UseGirderClip"  or WhichMovie == "EnemySpawnCuts" or WhichMovie == "CollectPart" then
    
        StartTurn()
        
    elseif WhichMovie == "StormTheCastleIntro" then
    
        
        --SendIntMessage("Worm.DieQuietly", 9)

       StartFirstTurn()
    end

end



--function Trigger_Destroyed()

--    TriggerIndex = GetData("Trigger.Index")
--    
--        
--    if TriggerIndex == 2 then -- Time machine bit has been destroyed.
--    
--        endmissionfail()
--        
--    end

--end

function Trigger_Collected()

    TriggerIndex = GetData("Trigger.Index")

    --if TriggerIndex == 2 then -- Time machine bit collected
    
        --if EnemyWormsDead == 8 then
        
            --endmissionsuccess()
        
        --end
        
    if TriggerIndex == 1 then
    
        TriggerCollected = true
        
    end
end

function SpawnWizardBack()

    lib_SetupWorm(14, "The King2")

    SendIntMessage("Worm.Respawn", 14) 
    lib_CreateEmitter("WXPL_WizardSpawner", "Wizzard")
end


function endmissionsuccess()
    SetData("EFMV.GameOverMovie", "OutroSuccess")
    SendMessage("GameLogic.Mission.Success") 
    lib_DisplaySuccessComment()
end


function endmissionfail()
    SendMessage("GameLogic.Mission.Failure")  
    lib_DisplayFailureComment()  
end

