-- fixed prof version

function Initialise()

	SetData("TurnTime", 60000)
	SetData("RoundTime", 3000000)

	
    -- teams, worm and telepads cloned from d/bank
    
	lib_SetupTeam(0, "HumanTeam")
    lib_SetupTeam(1, "BaddieTeam")
    lib_SetupTeam(2, "ProfTeam")
    
	lib_SetupWorm(0, "Player")
    lib_SetupWorm(1, "Player2")
	lib_SetupWorm(2, "Player3")
	lib_SetupWorm(3, "Prof")
    
    lib_SetupWorm(4, "Baddie")
    lib_SetupWorm(5, "Baddie2")
	lib_SetupWorm(6, "Baddie3")
	lib_SetupWorm(7, "Baddie4")
    
	SendMessage("WormManager.Reinitialise")

    WormAILevel = "AIParams.CPU2"        
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm05") 
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07")
       
        
	lib_SetupTeamInventory(0, "Inv_Human")
    lib_SetupTeamWeaponDelays(0, "HumanWeaponDelays")
    
    lib_SetupTeamInventory(1, "Inv_Baddie")
    lib_SetupTeamWeaponDelays(1, "AIWeapDelays")

    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 3000         
    CloseContainer(lock)     
    local CrateArray = { "Crate1", "Crate2", "Crate3", "Crate4", "Crate5", 
				    "Crate6", "Crate7", "Crate8", "Crate9", "Jewels", "EasterEgg"} -- need to add easter egg here

    local EasterEgg= QueryContainer( "Lock.EasterEgg.3")
    if EasterEgg.State == "Unlocked"  then
				    
	for i = 1,10 do
	     lib_SpawnCrate(CrateArray[i])
	end
    else
	for i = 1,11 do
	     lib_SpawnCrate(CrateArray[i])
	end
    end
    
    
    SendIntMessage("Crate.RadarHide", 11) 
    
    Emitter1 = lib_CreateEmitter("WXPL_JewelTwinkle", "Twinkle1") 
    Emitter2 = lib_CreateEmitter("WXPL_JewelTwinkle", "Twinkle2") 
    Emitter3 = lib_CreateEmitter("WXP_TimePieces", "EasterEgg") 
    
	PlayIntroMovie()
    
    HumanDead = 0
    BaddieDead = 0
    AddStrike = 0
    CrateColl = 0
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "CarpetCapersIntro")
    SendMessage("EFMV.Play")
end


function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "CarpetCapersIntro" then
        
        SendIntMessage("Crate.Delete", 10)
        
        lib_DeleteEmitterImmediate(Emitter1)
        lib_DeleteEmitterImmediate(Emitter2)
        
        StartFirstTurn()
    
--    elseif WhichMovie == "KillEnemy" then
--    
--        SendMessage("Commentary.EnableDefault")
    end
end

function TurnStarted()
    Team = GetData("CurrentTeamIndex")
    
    -- on turn 21 the baddies get strike weapons
    if Team == 1 then
        AddStrike = AddStrike + 1
        
        if AddStrike == 20 then
        
            lib_Comment("M.Arab.Carpet.COM.1")
        
        elseif AddStrike == 21 then
        
            local DataID = lib_GetTeamInventoryName(1)
            lock, inventory = EditContainer(DataID) 
            inventory.Airstrike = 10
            CloseContainer(lock)
        end
    end
end


function TurnEnded()

        -- if the time expires then the mission is failed
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
    
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    elseif BaddieDead == 4 then
    
            SetData("EFMV.GameOverMovie", "Outro")
            SendMessage("GameLogic.Mission.Success")
            lib_DisplaySuccessComment()
    
    else    
    
        StartTurn()
    end
end


function SetWind()

   SetData("Wind.Speed", 0)

end



function Crate_Collected()
    
    --if player collects all 9 crates then removed
    --utilities from inventory
    CrateIndex = GetData("Crate.Index")
    
    if CrateIndex < 11 then
    
        CrateColl = CrateColl + 1
    end
    
--    if CrateColl == 2 then
--    
--        SendMessage("Commentary.NoDefault")
    
    if CrateColl == 3 then
        
        SetData("EFMV.MovieName", "KillEnemy")
        SendMessage("EFMV.Play")
                
    elseif CrateIndex == 11 then
        
        lib_DeleteEmitterImmediate(Emitter3)
        
        SendStringMessage("WXMsg.EasterEggFound", "Lock.EasterEgg.3") -- unlock easter egg

        SetData("CommentaryPanel.Delay", 3000) -- 5 seconds
        SetData("CommentaryPanel.Comment", "M.Easter.1")-- You have found a easter egg
        SendMessage("CommentaryPanel.TimedText")
    end
end

function Worm_Died()

    --if dead is human then fail else win!
    deadWorm = GetData("DeadWorm.Id")
    
    if deadWorm == 3 then 

        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    elseif deadWorm > 3 then
    
        BaddieDead = BaddieDead + 1
        
        
    else --if deadWorm == 0 or deadWorm == 1 or deadWorm == 3 then
        
        HumanDead = HumanDead + 1
    
    end
    
    if HumanDead == 3 then 
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    end
end

