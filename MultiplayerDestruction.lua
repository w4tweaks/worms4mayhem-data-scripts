function Initialise()

    lib_SetupMultiplayer()

    SendMessage("WormManager.Reinitialise")
    lib_SetupMinesAndOildrums() -- do this after worms are set up to get correct worm collision

    g_nInitialLand = {0, 0, 0, 0}
    g_sIslandCode = {"TM00", "TM01", "TM02", "TM03"}

    for team = 0,3 do
        local TeamContainer = QueryContainer(lib_GetTeamContainerName(team))
        if TeamContainer.Active then
           SendStringMessage("Land.GetLandRemaining", g_sIslandCode[team+1] )
           local Land = GetData("Land.LandRemaining")
           g_nInitialLand[team+1] = Land 
        
           -- remap the spawn area for this team to be the centre of the map from now on
           SetData("SpawnVolume.CopyFrom", 4)
           SendIntMessage("SpawnVolume.Remap", team)
        end
    end
	
	SetData("DestructionTeamDied.0", 0)
	SetData("DestructionTeamDied.1", 0)
	SetData("DestructionTeamDied.2", 0)
    SetData("DestructionTeamDied.3", 0)
--    SetData("HUD.Counter.Active", 1) 
--    SetData("HUD.Counter.Percent", 1)

    StartFirstTurn()
end

function TurnStarted()

    UpdateCounter()

    SetData( "Turn.Boring", 0 )
    SetData( "Turn.MaxDamage", 1 )

end

function Land_NewShape()
    UpdateCounter()
end

function UpdateCounter()
    -- the counter next to the timer
    local team = GetData("CurrentTeamIndex")
    SetData("HUD.Counter.Value", GetTeamPercent(team))

    -- the values used by the health bars
    local Data = { "HUD.Counter.Team00", "HUD.Counter.Team01", "HUD.Counter.Team02", "HUD.Counter.Team03" }
    for team = 0,3 do
        if g_nInitialLand[team+1]>0 then
           SetData(Data[team+1], GetTeamPercent(team))
        end
    end
end

function GetTeamPercent(team)
    SendStringMessage("Land.GetLandRemaining", g_sIslandCode[team+1] )
    local Land = GetData("Land.LandRemaining")
    -- display 0% when 10%
    local ActualPercent = 100 * Land / g_nInitialLand[team+1]
    local DisplayedPercent
    if ActualPercent < 10 then
       DisplayedPercent = 0
    else
       DisplayedPercent = (ActualPercent - 10) / 0.9 
    end
    return DisplayedPercent
end

function DoOncePerTurnFunctions()

    -- Make sure the land values are up to date
    UpdateCounter()
    -- kill off teams with low amount of land
    local bTeamDied = false
    for team = 0,3 do
	   local teamAlreadyDead
	   local resourceName = 'DestructionTeamDied.' .. team
	   teamAlreadyDead = GetData(resourceName)
		
		if teamAlreadyDead == 0 and IsEnoughLandRemaining(team) == false then
           local WormIndex
           for WormIndex=0,15 do
               local worm = QueryContainer(lib_GetWormContainerName(WormIndex))
               if worm.TeamIndex == team then
                   SendIntMessage("WXWormManager.UnspawnWorm", WormIndex)
               end
               bTeamDied = true
				SetData(resourceName, 1)
           end
        end
    end

    -- no crates if a team just died
    if bTeamDied == false then
       SendMessage("GameLogic.DropRandomCrate")
    end
end

function IsEnoughLandRemaining(team)
   SendStringMessage("Land.GetLandRemaining", g_sIslandCode[team+1] )
   local Land = GetData("Land.LandRemaining")
   if Land < g_nInitialLand[team+1]*0.1 then
      return false
   else
      return true
   end
end

function Worm_Died()
    -- spawn a replacement if there is still enough land left
    local WormIndex = GetData("DeadWorm.Id")
    local WormContainer = lib_QueryWormContainer(WormIndex)
    local team = WormContainer.TeamIndex
    if IsEnoughLandRemaining(team) then
        local scheme = QueryContainer("GM.SchemeData")
        local lock, EditWorm = EditContainer(lib_GetWormContainerName(WormIndex))
        EditWorm.Energy = scheme.WormHealth
        EditWorm.Active = true
        CloseContainer(lock)
        SendIntMessage("Worm.Respawn", WormIndex)
    end
end

function TurnEnded()
   UpdateCounter()
   local RoundTimeRemaining = GetData("RoundTimeRemaining")
   if RoundTimeRemaining == 0 then
      SendMessage("GameLogic.Draw")
   else
      CheckOneTeamVictory()      
   end
end
