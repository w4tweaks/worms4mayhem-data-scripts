function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
    SetData("RoundTime",0)
	SetData("HotSeatTime", 0) 
    SetData("RetreatTime", 0) 
    SetData("DefaultRetreatTime",0)
    SetData("PostActivityTime",0)
    SetData("HUD.Clock.DisplayTenths", 1)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Human Team")
	lib_SetupWorm(0, "Player")
	lib_SetupTeamInventory(0, "Inv Human")
	
	SendMessage("WormManager.Reinitialise")
		
    g_nNextTarget = 1
    TotalTargetNumber = 10
    
    SetData("HUD.Counter.Active", 1)
    SetData("HUD.Counter.Value", 10)

	
    SpawnNextTarget()
   -- StartFirstTurn()
   PlayIntroMovie()
   
end

function TurnStarted()
   SendMessage("Weapon.Create")
end

function Crate_Destroyed()

    TotalTargetNumber = TotalTargetNumber - 1 

    SetData("HUD.Counter.Value", TotalTargetNumber)
    
    TextToDisplay = {"C.Generic.Good.1","C.Generic.Good.2","C.Generic.Good.3","C.Generic.Time.1"}
        local myRandomInteger = lib_GetRandom(1, 4)
        
        lib_Comment(TextToDisplay[myRandomInteger])

    SpawnNextTarget()

end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
end

function SpawnNextTarget()

    if g_nNextTarget == 11 then
        --lib_DisplaySuccessComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Success")
        
    else
    
    -- target cycle 
    local Crate = {"Targ1", "Targ2", "Targ3", "Targ4", "Targ5", "Targ6", 
    				"Targ7", "Targ8", "Targ9", "Targ10"}
        lib_SpawnCrate(Crate[g_nNextTarget])
        g_nNextTarget = g_nNextTarget + 1
        
        log("g_nNextTarget = ", g_nNextTarget)
        
    end

end


function TurnEnded()

    StartTurn()

end

function Worm_Damaged_Current()
  SetData("EFMV.GameOverMovie", "Outro")
  SendMessage("GameLogic.Challenge.Failure")
  lib_DisplayFailureComment()
end

