function Initialise()
	
	--SetData("Land.Indestructable", 1)
	
    SetData("TurnTime", 0) -- one long turn 
    
    SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 1000)
	SetData("Mine.MaxFuse", 1000)
    --SendMessage("GameLogic.PlaceObjects")
    
    --set up triggers, dynamite crate, teams, worms and 
    --inventories from databank
    
    --SetData("Trigger.Visibility", 1)
	
    lib_SpawnTrigger("TankTrigger_1")
	lib_SpawnTrigger("TankTrigger_2")
	lib_SpawnTrigger("TankTrigger_3")
   
   
    lib_SpawnCrate("Crate3")
--~     lib_SpawnCrate("Crate0")
    lib_SpawnCrate("Crate9")
    --lib_SpawnCrate("Crate4")
    
    g_bWarehouseCratecollected = false
	
	lib_SetupTeam(0, "PlayerTeam")
	lib_SetupTeam(1, "CPUTeam")
	
	lib_SetupWorm(0, "PlayerWorm")
	
        WormAILevel = "AIParams.CPU1"
        CopyContainer(WormAILevel, "AIParams.Worm01")
        CopyContainer(WormAILevel, "AIParams.Worm02")        
        
	SendMessage("WormManager.Reinitialise")
	
        
	lib_SetupTeamInventory(0, "PlayerInventory")
	lib_SetupTeamInventory(1, "CPUInventory")
    
    lib_SpawnCrate("Targ1")
    lib_SpawnCrate("Targ2")
    lib_SpawnCrate("Targ3")
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 0          
    CloseContainer(lock)

    
    --declare variables here`
	EnemyDead = 0
	g_nDiggersGot = 0
    Crate3Gone = false
    GotTwoDiggers = false
    GotThreeDiggers = false
    g_nStage = 0
    g_bPylonCrateCollected = false
    DiggerHasNotBeenblownUp = false
    
	SetData("EFMV.MovieName", "Intro")
	SendMessage("EFMV.Play") 
    
    NumberOfMoviesPlayed = 0
    
end

function EFMV_Terminated()
    
    --checks which move has been played and sets events for conditions
    local WhichMovie = GetData("EFMV.MovieName")
    NumberOfMoviesPlayed = NumberOfMoviesPlayed + 1
    
    if WhichMovie == "Intro" then
    
        StartFirstTurn()
    
    elseif WhichMovie == "DiggerBlow1" then
        ThreeDiggersHit()
    elseif WhichMovie == "DiggerBlow2" then
        ThreeDiggersHit()
    elseif WhichMovie == "DiggerBlow3" then
        ThreeDiggersHit()
    
    elseif WhichMovie == "Midtro" then
        
        lib_SpawnCrate("Crate1")
		lib_SpawnCrate("Crate2")
        lib_SpawnCrate("Crate5")
        lib_SpawnCrate("Crate6")
        lib_SpawnCrate("Crate7")
        lib_SpawnCrate("Crate8")
        
        SetData("TurnTime", 90000) -- set to turn based 
        
    end
    
    if NumberOfMoviesPlayed == 2 then
    
        lib_SpawnCrate("Crate4")
    end
end

function ThreeDiggersHit()
    --if all 3 diggers have been hit then play midtro
    if GotThreeDiggers == true then

        PlayMidtro()
    else
        -- remove the borders if we are not continuing straight into the midtro
        SendMessage("EFMV.End")
    end
end

function Crate_Collected()

    CrateIndex = GetData("Crate.Index")
    
    if CrateIndex == 0 or CrateIndex == 3 then
    
        g_nStage = 1

    end
    
    if CrateIndex == 0 then
        
        g_bWarehouseCratecollected = true
        
    
    elseif CrateIndex == 4 then
        
        g_bPylonCrateCollected = true
        
    end
    
    if CrateIndex == 3 then
        
        Crate3Gone =  true
    end
end
    
function Crate_Sunk()
    CrateIndex = GetData("Crate.Index")
    
    --if crate3 falls into water, set to true
    if CrateIndex == 3 then
    
        Crate3Gone = true
    end
end


function Trigger_Destroyed() 
    
    TriggerIndex = GetData("Trigger.Index")
    g_nDiggersGot = g_nDiggersGot + 1
    
    DiggerHasNotBeenblownUp = true
    
    if TriggerIndex == 0 then
        
        --lib_CreateEmitter("WXPL_DamagedDigger", "Trig0")
        SendIntMessage("Crate.Delete", 5)
        SetData("EFMV.MovieName", "DiggerBlow1")
        SendMessage("EFMV.Play") 
    
    elseif TriggerIndex == 1 then
    
        --lib_CreateEmitter("WXPL_DamagedDigger", "Trig1")
        SendIntMessage("Crate.Delete", 6)
        SetData("EFMV.MovieName", "DiggerBlow2")
        SendMessage("EFMV.Play") 
        
    elseif TriggerIndex == 2 then
        
        --lib_CreateEmitter("WXPL_DamagedDigger", "Trig2")
        SendIntMessage("Crate.Delete", 7)
        SetData("EFMV.MovieName", "DiggerBlow3")
        SendMessage("EFMV.Play") 
        
    end
        
        
    -- when 2 diggers have been blown up then spawn the last
    --dynamite crate up. when 3 have been got spawn the weapon
    --crates in, send an alert message and then spawn the guard
    --worms in
    
    if g_nDiggersGot == 1 then
        
        lib_Comment("One down, two to go!")
    
    elseif g_nDiggersGot == 2 then
        
        g_nStage = 2
        GotTwoDiggers = true
        
    
	elseif g_nDiggersGot == 3 then  
        
        GotThreeDiggers = true
		
        lib_SetupWorm(1, "GuardWorm_1")
        SendIntMessage("Worm.Respawn", 1)    
        lib_SetupWorm(2, "GuardWorm_2")
        SendIntMessage("Worm.Respawn", 2)
	end
end

function PlayMidtro()

    SetData("EFMV.MovieName", "Midtro")
    SendMessage("EFMV.Play") 
end


function Worm_Died()
    
    --if an enemy dies then add 1 to the e/dead variable.     
 	deadWorm = GetData("DeadWorm.Id")
    
--    if deadWorm == 0 then
--    
--        
--        lib_DisplayFailureComment()
--        SendMessage("GameLogic.Mission.Failure")
    
	if deadWorm == 1 or deadWorm == 2 then
        
        EnemyDead = EnemyDead + 1
    
    end
end



function TurnEnded()

        local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            SetData("GameToFrontEndDelayTime",1000)
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        else
    
        -- win and lose conditions 
        TeamCount = lib_GetActiveAlliances()
        WhichTeam = lib_GetSurvivingTeamIndex()
    
            if TeamCount == 0 or (WhichTeam == 1 and TeamCount == 1) then
            
                SetData("GameToFrontEndDelayTime",1000)
                SendMessage("GameLogic.Mission.Failure")
                lib_DisplayFailureComment()
    
            elseif EnemyDead == 2 then
        
                SetData("EFMV.GameOverMovie", "Outro")
                
                SendMessage("GameLogic.Mission.Success")    
                lib_DisplaySuccessComment()
    
        else 
       
            StartTurn()
        
        end
    end
end
    

function DoOncePerTurnFunctions()

    -- Decide whether to spawn a new crate
    
        local inventory = QueryContainer( lib_GetAllianceInventoryName(0) )
        
    if inventory.Dynamite == 0 and DiggerHasNotBeenblownUp == false then
    
        SetData("CommentaryPanel.Delay", 3000)
        SetData("CommentaryPanel.Comment", "M.Con.Building.NEW.1")
        SendMessage("CommentaryPanel.TimedText")
        DiggerHasNotBeenblownUp = true
        
    end

    if g_bWarehouseCratecollected == true and Crate3Gone == true and GotTwoDiggers == false and g_nStage == 1 then
    
        local inventory = QueryContainer( lib_GetAllianceInventoryName(0) )
        if inventory.Dynamite == 0 then
            

            
            lib_SpawnCrate("Crate0")
            
            g_bWarehouseCratecollected = false
        end
  
  
    elseif g_bPylonCrateCollected == true and g_nStage == 2 and GotThreeDiggers == false then
        
        local inventory = QueryContainer( lib_GetAllianceInventoryName(0) )
        if inventory.Dynamite == 0 then
--            
            SetData("CommentaryPanel.Delay", 3000)
            SetData("CommentaryPanel.Comment", "M.Con.Building.NEW.2")
            SendMessage("CommentaryPanel.TimedText")
--            
            lib_SpawnCrate("Crate4")
            
            g_bPylonCrateCollected = false
        end
  
    end
  
end
	
