


-- Mission Basic setup 1v1 player

function Initialise()

-- Sets up mines and turn time
    --SetData("Mine.DudProbability", 0.1)
    --SetData("Mine.MinFuse", 1000)
    --SetData("Mine.MaxFuse", 5000)

    SetupWormsAndTeams()
    SetupInventories()
    --SpawnCrate()

    SetData("Camera.StartOfTurnCamera", "Default")
    StartFirstTurn()
    -- test random crate code (to check when new weapons are added, that they're also added to the scheme container) - PNT
    lock, scheme = EditContainer("GM.SchemeData")
    scheme.HealthChance = 1
    scheme.WeaponChance = 1
    scheme.UtilityChance = 1
    scheme.MysteryChance = 1
    CloseContainer(lock)

--~     SendMessage("GameLogic.CrateShower")
--~     SendMessage("GameLogic.CrateShower")
--~     SendMessage("GameLogic.CrateShower")
    SendMessage("GameLogic.DropRandomCrate")
    SendMessage("GameLogic.CreateRandMineFactory")
    SendMessage("GameLogic.CreateRandomTelepad")

   SetWormpotModes()

end




function SetupWormsAndTeams()
-- Activate Team 0
    lock, team = EditContainer("Team.Data00") 
    team.Active = true
    team.Name = "Team 1"
    team.TeamColour = 0
-- team.IsAIControlled = true
    CloseContainer(lock) -- must close the container ASAP

-- Activate Team 1
    lock, team = EditContainer("Team.Data01") 
    team.Active = true
    team.Name = "Team 2"
    team.TeamColour = 1
-- team.IsAIControlled = true
    CloseContainer(lock)

-- Activate Team 2
    lock, team = EditContainer("Team.Data02") 
    team.Active = true
    team.Name = "Team 3"
    team.TeamColour = 2
-- team.IsAIControlled = true
    CloseContainer(lock) -- must close the container ASAP

-- Activate Team 3
    lock, team = EditContainer("Team.Data02") 
    team.Active = true
    team.Name = "Team 3"
    team.TeamColour = 2
-- team.IsAIControlled = true
    CloseContainer(lock) -- must close the container ASAP

-- Worm 0, Team 0
    lock, worm = EditContainer("Worm.Data00") 
    worm.Active = true
    worm.Name = "T1,W1"
    worm.Energy = 100
    worm.WeaponFuse = 3
    worm.WeaponIsBounceMax = false
    worm.TeamIndex = 0
    --worm.SfxBankName = "Classic"
    worm.Spawn = "spawn"
    CloseContainer(lock) 
-- Worm 1, Team 0
    CopyContainer("Worm.Data00", "Worm.Data01")
    lock, worm = EditContainer("Worm.Data01") 
    worm.Name = "T1,W2"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 2, Team 0
    CopyContainer("Worm.Data00", "Worm.Data02")
    lock, worm = EditContainer("Worm.Data02") 
    worm.Name = "T1,W3"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 3, Team 0
    CopyContainer("Worm.Data00", "Worm.Data03")
    lock, worm = EditContainer("Worm.Data03") 
    worm.Name = "T1,W4"
    worm.Spawn = "spawn"
    CloseContainer(lock)

-- Worm 4, Team 1
    CopyContainer("Worm.Data00", "Worm.Data04")
    lock, worm = EditContainer("Worm.Data04")
    worm.Energy = 80 
    worm.Name = "T2,W1"
    worm.TeamIndex = 1
    --worm.SfxBankName = "Classic"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 5, Team 1
    CopyContainer("Worm.Data04", "Worm.Data05")
    lock, worm = EditContainer("Worm.Data05") 
    worm.Name = "T2,W2"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 6, Team 1
    CopyContainer("Worm.Data04", "Worm.Data06")
    lock, worm = EditContainer("Worm.Data06") 
    worm.Name = "T2,W3"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 7, Team 1
    CopyContainer("Worm.Data04", "Worm.Data07")
    lock, worm = EditContainer("Worm.Data07") 
    worm.Name = "T2,W4"
    worm.Spawn = "spawn"
    CloseContainer(lock)

-- Worm 8, Team 2
    CopyContainer("Worm.Data00", "Worm.Data08")
    lock, worm = EditContainer("Worm.Data08")
    worm.Energy = 80 
    worm.Name = "T3,W1"
    worm.TeamIndex = 2
    --worm.SfxBankName = "Classic"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 9, Team 2
    CopyContainer("Worm.Data08", "Worm.Data09")
    lock, worm = EditContainer("Worm.Data09") 
    worm.Name = "T3,W2"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 10, Team 2
    CopyContainer("Worm.Data08", "Worm.Data10")
    lock, worm = EditContainer("Worm.Data10") 
    worm.Name = "T3,W3"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 11, Team 2
    CopyContainer("Worm.Data08", "Worm.Data11")
    lock, worm = EditContainer("Worm.Data11") 
    worm.Name = "T3,W4"
    worm.Spawn = "spawn"
    CloseContainer(lock)

-- Worm 12, Team 3
    CopyContainer("Worm.Data00", "Worm.Data12")
    lock, worm = EditContainer("Worm.Data12")
    worm.Energy = 80 
    worm.Name = "T4,W1"
    worm.TeamIndex = 3
    --worm.SfxBankName = "Classic"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 13, Team 3
    CopyContainer("Worm.Data12", "Worm.Data13")
    lock, worm = EditContainer("Worm.Data13") 
    worm.Name = "T4,W2"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 14, Team 3
    CopyContainer("Worm.Data12", "Worm.Data14")
    lock, worm = EditContainer("Worm.Data14") 
    worm.Name = "T4,W3"
    worm.Spawn = "spawn"
    CloseContainer(lock)
-- Worm 15, Team 3
    CopyContainer("Worm.Data12", "Worm.Data15")
    lock, worm = EditContainer("Worm.Data15") 
    worm.Name = "T4,W4"
    worm.Spawn = "spawn"
    CloseContainer(lock)

    SendMessage("WormManager.Reinitialise")
end

function SetupInventories()
-- sets up a default container and adds our selection to it
    lock, inventory = EditContainer("Inventory.Team.Default") 
    inventory.Bazooka = -1
    inventory.Grenade = -1
    inventory.ClusterGrenade =-1
    inventory.Airstrike = -1
    inventory.Dynamite = -1
    inventory.HolyHandGrenade = -1
    inventory.BananaBomb = -1
    inventory.Landmine = -1
    inventory.HomingMissile = -1
    inventory.Sheep = -1
    inventory.SuperSheep = -1
    inventory.Parachute = -1
    inventory.Jetpack = -1
    inventory.SkipGo = -1
    inventory.OldWoman = -1
    inventory.Girder = -1
    -- inventory.BridgeKit = -1
    inventory.Shotgun = -1
    inventory.GasCanister = -1
    inventory.NinjaRope = -1
    inventory.FirePunch = -1
    inventory.Prod = -1
    inventory.ConcreteDonkey = -1
    inventory.BaseballBat = -1
    inventory.Flood = -1
    inventory.Redbull = -1
    inventory.WeaponFactoryWeapon = -1
    inventory.Starburst = -1
    inventory.ChangeWorm = -1
    inventory.Surrender = -1
    inventory.SentryGun = -1
    inventory.Fatkins = -1
    inventory.PoisonArrow = -1
    inventory.Scouser = -1
    inventory.BubbleTrouble = -1
    inventory.NoMoreNails = -1
    inventory.SuperAirstrike = -1
    inventory.AlienAbduction = -1
    inventory.SniperRifle = -1
    

    CloseContainer(lock) -- must close the container ASAP

    lock, weapon = EditContainer("kWeaponSuperSheep")
    weapon.LifeTime = -1
    CloseContainer(lock)

-- Copies this selection into each worm
   CopyContainer("Inventory.Team.Default", "Inventory.Team00")
   CopyContainer("Inventory.Team.Default", "Inventory.Team01")
   CopyContainer("Inventory.Team.Default", "Inventory.Team02")
   CopyContainer("Inventory.Team.Default", "Inventory.Team03")
end

function Crate_Collected()
end

function DoWormpotOncePerTurnFunctions()
end

function SpawnCrate()
    SetData("Crate.Spawn", "CrateSpawn1")
    SetData("Crate.Type", "Utility")
    SetData("Crate.Contents", "kUtilityCrateShower") 
    SetData("Crate.NumContents", 1)
    SetData("Crate.Index", 0)
    SetData("Crate.GroundSnap", 1)
    SendMessage("GameLogic.CreateCrate")
end

function DoOncePerTurnFunctions()
       SendMessage("GameLogic.StartMineFactory")
   DoWormpotOncePerTurnFunctions()
end

