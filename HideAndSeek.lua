
    
function Initialise()

    SetData("Land.Indestructable", 1)

    -- 4 worms each with 100 health, Allied
    lib_QuickSetupMultiplayer(4, 100, true)

    lib_SetupTeamInventory(0, "CollectorInventory")
    lib_SetupTeamInventory(1, "CollectorInventory")
    lib_SetupTeamInventory(2, "HunterInventory")
    lib_SetupTeamInventory(3, "HunterInventory")

    -- Spawn 19 empty crates and one Grenade Crate
    CrateSpawn = {"crate1", "crate2", "crate3", "crate4", "crate5", 
                  "crate6", "crate7", "crate8", "crate9", "crate10", 
                  "crate11", "crate12", "crate13", "crate14", "crate15", 
                  "crate16", "crate17", "crate18", "crate19" }
    local n
    for n = 1, 19 do
       lib_SetupCrate("EmptyCrate")
       SetData("Crate.Spawn", CrateSpawn[n])
       SendMessage("GameLogic.CreateCrate")
    end
    lib_SpawnCrate("GrenadeCrate")


    g_nCratesCollected = 0

    StartFirstTurn()

end

function Crate_Collected()
    g_nCratesCollected = g_nCratesCollected + 1
    if g_nCratesCollected == 20 then
        SendIntMessage("GameLogic.Win", 0) -- award the win to team 0
    end
end

function TurnEnded()
    CheckOneTeamVictory() -- standard deathmatch rules in addition
end
