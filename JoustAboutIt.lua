function Initialise()
	
	SetData("Land.Indestructable", 1)
    
    --SetData("RoundTime", 60000)
    SetData("TurnTime", 45000)
	SendMessage("GameLogic.PlaceObjects")
	
	-- setup teams, worms and inventories from databank
	lib_SetupTeam(0, "Human_Team")
	lib_SetupTeam(1, "AI_Team")
	lib_SetupTeam(2, "Wizard_Team")
	
    lib_SetupWorm(1, "Worm1")
    lib_SetupWorm(2, "Worm2")
    lib_SetupWorm(3, "Worm3")
    
    lib_SetupWorm(4, "Enemy1")
    lib_SetupWorm(5, "Enemy2")
    lib_SetupWorm(6, "Enemy3")
    
    lib_SetupWorm(13, "Wizard")
    
    WormAILevel = "AIParams.CPU2"
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm05")
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07")
    CopyContainer(WormAILevel, "AIParams.Worm08")
    CopyContainer(WormAILevel, "AIParams.Worm09")
    CopyContainer(WormAILevel, "AIParams.Worm10")
    CopyContainer(WormAILevel, "AIParams.Worm11")
    CopyContainer(WormAILevel, "AIParams.Worm12")
    CopyContainer(WormAILevel, "AIParams.Worm13")
    
    SendMessage("WormManager.Reinitialise")
	
	lib_SetupTeamInventory(0, "HumanWeapz")
	lib_SetupTeamInventory(1, "AIWeapz")
	lib_SpawnCrate("Crate0")
    
    lib_SpawnCrate("HourGlass")
    Emitter = lib_CreateEmitter("WXP_TimePieces", "Glow")
    
    SecondWaveOfWormsHaveSpawnedIn = false
    ThirdWaveOfWormsHaveSpawnedIn = false
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 2000         
    CloseContainer(lock)
    
    DoNotPlayCollectPart = false
    g_HumanDead = 0
    g_EnemyDead = 0
    g_nNextCrate = 1
    AllHumanWormsDead = false
    HealthCrateCollected = false
    
	PlayIntroMovie()
end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()

   local LastMovie = GetData("EFMV.MovieName")
   if LastMovie=="Intro" then
        
        SendIntMessage("Crate.Delete", 1)
        lib_DeleteEmitterImmediate(Emitter)
        SendIntMessage("Worm.DieQuietly", 13)         
    
        StartFirstTurn()
    elseif LastMovie == "CollectPart" then
    
        StartTurn()
    
    end
    
end

function PlayMidtro()
    
    SetData("EFMV.MovieName", "Midtro")
    SendMessage("EFMV.Play")
    
end


function PlayMidtro2()

    SetData("EFMV.MovieName", "Midtro2")
    SendMessage("EFMV.Play")
end


function PlayCollectPart()

    if DoNotPlayCollectPart == false then
        SetData("EFMV.MovieName", "CollectPart")
        SendMessage("EFMV.Play")
        DoNotPlayCollectPart = true
    end
    
end


function Worm_Died()
    
	--if its a dead enemy worm then +1 to the variable
	deadWorm = GetData("DeadWorm.Id")
    
	if deadWorm >3 then
	
		g_EnemyDead = g_EnemyDead + 1
    
    elseif deadWorm == 1 or deadWorm == 2 or deadWorm == 3 then
    
        g_HumanDead = g_HumanDead + 1
        
	end
    
    	-- spawn two more nme when 1st is killed and then another 3 after 
	--the first 3 are wiped out
    if g_EnemyDead == 4 and SecondWaveOfWormsHaveSpawnedIn == false then
	
		lib_SetupWorm(7, "Enemy4") 
		lib_SetupWorm(8, "Enemy5")
        lib_SetupWorm(9, "Enemy6") 
		SendIntMessage("Worm.Respawn", 7)  
		SendIntMessage("Worm.Respawn", 8)
        SendIntMessage("Worm.Respawn", 9)  
        
        local DataID = lib_GetTeamInventoryName(0)
        lock, inventory = EditContainer(DataID) 
        inventory.BaseballBat = 0
        inventory.FirePunch = 0
        inventory.Prod = 0
        inventory.NinjaRope = 0
        inventory.Bazooka = 10
        inventory.HomingMissile = 1
        inventory.Grenade = -1
        CloseContainer(lock)
        
        local DataID = lib_GetTeamInventoryName(1)
        lock, inventory = EditContainer(DataID) 
        inventory.BaseballBat = 0
        inventory.FirePunch = 0
        inventory.Prod = 0
        inventory.Bazooka = 10
        inventory.HomingMissile = 1
        inventory.Grenade = -1
        CloseContainer(lock)
        
        SecondWaveOfWormsHaveSpawnedIn = true 
        PlayMidtro()
    end
	
    if g_EnemyDead == 7 and ThirdWaveOfWormsHaveSpawnedIn == false then 
		
	
		lib_SetupWorm(10, "Enemy7")
		lib_SetupWorm(11, "Enemy8") 
        lib_SetupWorm(12, "Enemy9") 
		SendIntMessage("Worm.Respawn", 10)  
		SendIntMessage("Worm.Respawn", 11)
        SendIntMessage("Worm.Respawn", 12)
        
        local DataID = lib_GetTeamInventoryName(0)
        lock, inventory = EditContainer(DataID) 
   
        inventory.Bazooka = 0
        inventory.HomingMissile = 0
        inventory.Grenade = 0
        inventory.Shotgun = -1
        inventory.PoisonArrow = 3
        inventory.SniperRifle = 3
        CloseContainer(lock)
        
        local DataID = lib_GetTeamInventoryName(1)
        lock, inventory = EditContainer(DataID) 
   
        inventory.Bazooka = 0
        inventory.HomingMissile = 0
        inventory.Grenade = 0
        inventory.Shotgun = -1
        inventory.PoisonArrow = 3
        inventory.SniperRifle = 3
        CloseContainer(lock)
        
        ThirdWaveOfWormsHaveSpawnedIn = true
        
        PlayMidtro2()
    end
    
    if g_EnemyDead == 10 then
        
        lib_SpawnCrate("HourGlass")
        Emitter = lib_CreateEmitter("WXP_TimePieces", "Glow")
        PlayCollectPart()
    
    elseif g_HumanDead == 3 then
        
        AllHumanWormsDead = true
    end
        
end

function Crate_Collected()
    
    CrateIndex = GetData("Crate.Index")
    
    if CrateIndex == 1 then
        
        DoNotPlayCollectPart = true
        lib_DeleteEmitterImmediate(Emitter)
        lib_SetupWorm(13, "Wizard")
        SendIntMessage("Worm.Respawn", 13)
        
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    
    else
        HealthCrateCollected = true
    end
end


function SpawnNextCrate()
    
    -- crate weapon cycle 
    local Crate = {"Crate0", "Crate1", "Crate2", "Crate3"}
        lib_SpawnCrate(Crate[g_nNextCrate])
        g_nNextCrate = g_nNextCrate + 1
        
    if g_nNextCrate>4 then 
        g_nNextCrate = 1 
    end
    
end

function TurnEnded()
    
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    elseif AllHumanWormsDead == true then
    
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    else
    
        StartTurn()
    end
end

function DoOncePerTurnFunctions()

    if HealthCrateCollected == true then
    
        SpawnNextCrate()
    end
    
    HealthCrateCollected = false
end
	
	