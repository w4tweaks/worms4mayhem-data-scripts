
-- Mission Chute To Victory!


function Initialise()	

    SetData("Land.Indestructable", 1)	
    SetData("TurnTime", 25000)
	SetData("RoundTime", 3000000)
    
    	
    SetData("Mine.DetonationType", 1) 
    SetData("Mine.MinFuse", 5000)
	SetData("Mine.MaxFuse", 5000)
    
    SendMessage("GameLogic.PlaceObjects")
    
	SetData("Trigger.Visibility", 0)
	lib_SpawnTrigger("Trig1")
    lib_SpawnTrigger("Trig2")
    lib_SpawnTrigger("Trig3")
    lib_SpawnTrigger("Trig4")
    lib_SpawnTrigger("Trig5")
--~     lib_SpawnTrigger("Trig6")

    lib_SpawnCrate("ParachuteCrate1") 
--~     lib_SpawnCrate("ParachuteCrate2")  
    lib_SpawnCrate("ParachuteCrate3") 
    lib_SpawnCrate("ParachuteCrate4") 
    lib_SpawnCrate("ParachuteCrate5")     

    lib_SetupTeam(0, "PlayerTeam")
    lib_SetupTeam(1, "CPUTeam")
    lib_SetupWorm(0, "PlayerWorm")  
    lib_SetupWorm(1, "CPUWorm_1")  
    lib_SetupWorm(2, "CPUWorm_2")  
    lib_SetupWorm(3, "CPUWorm_3")  
    lib_SetupWorm(4, "CPUWorm_4")  


    SendMessage("WormManager.Reinitialise")

    WormAILevel = "AIParams.CPU2"        
    CopyContainer(WormAILevel, "AIParams.Worm01")
    CopyContainer(WormAILevel, "AIParams.Worm02") 
    CopyContainer(WormAILevel, "AIParams.Worm03")
    CopyContainer(WormAILevel, "AIParams.Worm04") 
  
    
    lib_SetupTeamInventory(0, "PlayerInventory")
    lib_SetupTeamInventory(1, "CPUInventory")
    

    EnemyWormDead = 0
    PlayerisOntheFinalBit = false
    PlayerHasReachedGirderBit = false
    
    PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
end


function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
        StartFirstTurn()
    
    else
        StartTurn()
    end
end

function SetWind()
    SetData("Wind.Speed", 0.0)
	SetData("Wind.Direction", 0.0)
end


function Trigger_Collected()

	local TriggerIndex = GetData("Trigger.Index")  
    
    if TriggerIndex == 4 then
   
       PlayerisOntheFinalBit = true
        
    elseif TriggerIndex == 5 then
    
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success") 
        lib_DisplaySuccessComment()
    
    end    

end

function TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            --SetData("GameToFrontEndDelayTime",1000)
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        elseif EnemyWormDead == 4 then

            SetData("EFMV.MovieName", "Midtro")
            SendMessage("EFMV.Play")
    
        else
        
            StartTurn()
        
        end
        
        

end

function Worm_Died()	
		
    local WormId = GetData("DeadWorm.Id")
    if WormId == 0 then  
        SendMessage("GameLogic.Mission.Failure")     -- the players worm died
        lib_DisplayFailureComment()
    else
    
        EnemyWormDead = EnemyWormDead + 1
    
    end
end

--~ function Crate_Collected()

--~     IndexNumber = GetData("Crate.Index")

--~    if IndexNumber == 2 then
--~    
--~        PlayerHasReachedGirderBit = true
--~        
--~     end
--~    
--~ end








