
-- Mission Basic setup 1v1 player

function Initialise()

-- Sets up mines and turn time
    SetData("Mine.DudProbability", 0.1)
    SetData("Mine.MinFuse", 1000)
    SetData("Mine.MaxFuse", 5000)

    SetupWormsAndTeams()
    SetupInventories()

    SetData("Camera.StartOfTurnCamera", "Default")

    StartFirstTurn()
end




function SetupWormsAndTeams()
-- Activate Team 0
    lock, team = EditContainer("Team.Data00") 
    team.Active = true
    team.Name = "The Lightsiders"
    team.TeamColour = 0
    CloseContainer(lock) -- must close the container ASAP
-- Activate Team 1
    lock, team = EditContainer("Team.Data01") 
    team.Active = true
    team.Name = "The Darksiders"
    team.TeamColour = 1
    team.IsAIControlled = true
    CloseContainer(lock)
-- Worm 0, Team 0
    lock, worm = EditContainer("Worm.Data00") 
    worm.Active = true
    worm.Name = "Light1"
    worm.Energy = 100
    worm.WeaponFuse = 3
    worm.WeaponIsBounceMax = false
    worm.TeamIndex = 0
    --worm.SfxBankName = "Classic"
    worm.Spawn = "spawn"
    CloseContainer(lock) 
-- Worm 1, Team 0
    CopyContainer("Worm.Data00", "Worm.Data01")
    lock, worm = EditContainer("Worm.Data01") 
    worm.Name = "Light2"
    worm.Spawn = "spawn"
    CloseContainer(lock)

    CopyContainer("Worm.Data00", "Worm.Data02")
    lock, worm = EditContainer("Worm.Data02") 
    worm.Name = "Light3"
    worm.Spawn = "spawn"
    CloseContainer(lock)


    CopyContainer("Worm.Data00", "Worm.Data03")
    lock, worm = EditContainer("Worm.Data03") 
    worm.Name = "Light4"
    worm.Spawn = "spawn"
    CloseContainer(lock)

-- Worm 4, Team 1
    CopyContainer("Worm.Data00", "Worm.Data04")
    lock, worm = EditContainer("Worm.Data04")
    worm.Name = "Dark1"
    worm.TeamIndex = 1
    --worm.SfxBankName = "Classic"
    worm.Spawn = "spawn"
    CloseContainer(lock)


    CopyContainer("Worm.Data04", "Worm.Data05")
    lock, worm = EditContainer("Worm.Data05") 
    worm.Name = "Dark2"
    worm.Spawn = "spawn"
    CloseContainer(lock)

    CopyContainer("Worm.Data04", "Worm.Data06")
    lock, worm = EditContainer("Worm.Data06") 
    worm.Name = "Dark3"
    worm.Spawn = "spawn"
    CloseContainer(lock)



   SendMessage("WormManager.Reinitialise")
end

function SetupInventories()
-- sets up a default container and adds our selection to it
    lock, inventory = EditContainer("Inventory.Team.Default") 
    inventory.Bazooka = -1
    inventory.Shotgun = -1
    inventory.BaseballBat = -1
    inventory.Dynamite = -1
    inventory.Prod = -1
    inventory.SkipGo = -1
    inventory.Grenade = -1
    inventory.ClusterGrenade =-1
    inventory.Landmine = -1
    inventory.Parachute = -1
    -- 10 items
    
    inventory.Jetpack = -1
    --inventory.SuperSheep = -1
    inventory.Sheep = -1
    inventory.HomingMissile = 1
    --inventory.ChangeWorm = -1
    -- 15 items
    
    inventory.Surrender = -1
    --inventory.WeaponFactoryWeapon = -1
    --inventory.ConcreteDonkey = -1
    --inventory.Flood = -1
    inventory.HolyHandGrenade = 1
    -- 20 items

    inventory.GasCanister = 1
    inventory.Airstrike = 1
    --inventory.Girder = -1
    inventory.BananaBomb = 1
    --inventory.Redbull = -1
    inventory.SentryGun = 1
    -- 25 items
    
    inventory.OldWoman = 1
    inventory.Scouser = 1
    inventory.FirePunch = -1
    inventory.Fatkins = 1
    inventory.Starburst = 1
    --inventory.Girder = -1
    -- 30 items
    
    CloseContainer(lock) -- must close the container ASAP


-- Copies this selection into each worm
   CopyContainer("Inventory.Team.Default", "Inventory.Team00")
   CopyContainer("Inventory.Team.Default", "Inventory.Team01")
end


function Crate_Collected()
end

function DoWormpotOncePerTurnFunctions()
end
