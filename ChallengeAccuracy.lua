function Initialise()
    
    SetData("TurnTime", 0)
    SetData("RoundTime",0)
	SetData("HotSeatTime", 0) 
    SetData("RetreatTime", 0) 
    SetData("DefaultRetreatTime",0)
    SetData("PostActivityTime",10)
    SetData("HUD.Clock.DisplayTenths", 1)
    
    lib_SetupTeam(0, "Team Human")
    
    lib_SetupWorm(0, "Player")
    lib_SetupWorm(1, "Dummy1")
    lib_SetupWorm(2, "Dummy2")
    lib_SetupWorm(3, "Dummy3")
    lib_SetupWorm(4, "Dummy4")
    lib_SetupWorm(5, "Dummy5")
    lib_SetupWorm(6, "Dummy6")
    lib_SetupWorm(7, "Dummy7")
    lib_SetupWorm(8, "Dummy8")
    lib_SetupWorm(9, "Dummy9")
    lib_SetupWorm(10, "Dummy10")
    lib_SetupWorm(11, "Dummy11")
    lib_SetupWorm(12, "Dummy12")
  
    SendMessage("WormManager.Reinitialise")

    
    
    lib_SetupTeamInventory(0, "Inventory Human")
    
    lib_SpawnTrigger("Trig1")
    lib_SpawnTrigger("Trig2")
    lib_SpawnTrigger("Trig3")
    lib_SpawnTrigger("Trig4")
    lib_SpawnTrigger("Trig5")
    lib_SpawnTrigger("Trig6")
    lib_SpawnTrigger("Trig7")
    
    Worm1HasNotBeenPosioned = false
    Worm2HasNotBeenPosioned = false
    Worm3HasNotBeenPosioned = false
    Worm4HasNotBeenPosioned = false
    Worm5HasNotBeenPosioned = false
    Worm6HasNotBeenPosioned = false
    Worm7HasNotBeenPosioned = false
    Worm8HasNotBeenPosioned = false
    Worm9HasNotBeenPosioned = false
    Worm10HasNotBeenPosioned = false
    Worm11HasNotBeenPosioned = false
    Worm12HasNotBeenPosioned = false
    
    SetData("HUD.Counter.Active", 1)
    SetData("HUD.Counter.Value", 12)
    
    TrigCount = 0
    NumberOfWormsForCounter = 12
    Poisoned_Worms = 0
   
   -- StartFirstTurn()
   PlayIntroMovie()
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
end

function SetWind()
	-- wind is permanently at 0
	SetData("Wind.Direction", 3.14)
end


function Worm_Died()
    
    DeadWorm = GetData("DeadWorm.Id")
    
    if DeadWorm == 0 then
    
        lib_DisplayFailureComment()
        SetData("EFMV.GameOverMovie","Outro")
        SendMessage("GameLogic.Challenge.Failure")
        
        
    end
    
end

function TurnStarted()

   SendMessage("Weapon.Create")
   
end


function TurnEnded()

    StartTurn()

end



function Worm_Damaged()

        TextToDisplay = {"C.Generic.Good.1","C.Generic.Good.2","C.Generic.Good.3","C.Generic.Time.1"}
        local myRandomInteger = lib_GetRandom(1, 4)
        
        lib_Comment(TextToDisplay[myRandomInteger])
        
        
  

    local DamagedWorm = GetData("DamagedWorm.Id")

    if DamagedWorm == 1 and Worm1HasNotBeenPosioned == false then
    
        Worm1HasNotBeenPosioned = true
        
        DoTheCounterStuff()


    elseif DamagedWorm == 2 and Worm2HasNotBeenPosioned == false then
    
        Worm2HasNotBeenPosioned = true
        
        DoTheCounterStuff()
            
            
    elseif DamagedWorm == 3 and Worm3HasNotBeenPosioned == false then
    
        Worm3HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
        
    elseif DamagedWorm == 4 and Worm4HasNotBeenPosioned == false then
    
        Worm4HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 5 and Worm5HasNotBeenPosioned == false then
    
        Worm5HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 6 and Worm6HasNotBeenPosioned == false then
    
        Worm6HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 7 and Worm7HasNotBeenPosioned == false then
    
        Worm7HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 8 and Worm8HasNotBeenPosioned == false then
    
        Worm8HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 9 and Worm9HasNotBeenPosioned == false then
    
        Worm9HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 10 and Worm10HasNotBeenPosioned == false then
    
        Worm10HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 11 and Worm11HasNotBeenPosioned == false then
    
        Worm11HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    elseif DamagedWorm == 12 and Worm12HasNotBeenPosioned == false then
    
        Worm12HasNotBeenPosioned = true
        
        DoTheCounterStuff()
        
    end
        
    if Poisoned_Worms == 12 then 
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Success")
    
    end
end

function DoTheCounterStuff()

    NumberOfWormsForCounter = NumberOfWormsForCounter - 1
    Poisoned_Worms = Poisoned_Worms + 1
    SetData("HUD.Counter.Value", NumberOfWormsForCounter)
    
end    

    
    
    
    
    
    
    
    
    
    
