

function Initialise()

    -- These details are copied from the "databank" for this mission
    -- The players "Mission Team" name and so on will also be TIC
    lib_SetupTeam(0, "team0")
    lib_SetupTeam(1, "team1")
    
    lib_SetupWorm(0, "worm0")
    lib_SetupWorm(1, "worm1")

    -- Again cloned from the one in the databank
    lib_SetupTeamInventory(0, "inventory")
    lib_SetupTeamInventory(1, "inventory")

    SendMessage("WormManager.Reinitialise")

    lib_SpawnCrate("mycrate")

    SetData("TurnTime", 0)
    
    SetData("HUD.Counter.Active", 1)
    SetData("HUD.Counter.Percent",1)
    SetData("HUD.Counter.Value", 100)
    g_CratesCollected = 0

    StartFirstTurn()
end


function TurnEnded()

    StartTurn()

end

function Crate_Collected()
    lib_SpawnCrate("mycrate")
    SetData("HUD.Counter.Percent",0)
    g_CratesCollected = g_CratesCollected + 1
    SetData("HUD.Counter.Value", g_CratesCollected)
end

