-- Mission Demo


function Initialise()						

    SetData("TurnTime", 0)
    SetData("RoundTime", -1)
    SetData("Land.Indestructable", 0)
    SendMessage("GameLogic.PlaceObjects")

    lockscheme, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 0          
    CloseContainer(lockscheme)
	
	    
    lib_SpawnCrate("Crate1") 
    
    
    lib_SetupTeam(0, "TeamHuman")
    lib_SetupTeam(1, "TeamCPU")
    
   
    SendMessage("WormManager.Reinitialise")

    lib_SetupTeamInventory(0, "InventoryHuman")
    lib_SetupTeamInventory(1, "InventoryCPU")
    
    -- Set up AI level of Worms
    CopyContainer("AIParams.CPU1", "AIParams.Worm04") 
    CopyContainer("AIParams.CPU1", "AIParams.Worm05") 
    CopyContainer("AIParams.CPU1", "AIParams.Worm06") 
    CopyContainer("AIParams.CPU1", "AIParams.Worm07") 

    g_bDeathmatchStarted = 0
    g_bCrateStatus = 0
    g_nNextCrate = 1
    g_bFirstMoviePlayer = 0

    -- copy the speech banks across now
    local GM = QueryContainer("GM.GameInitData")
    for index = 0, 15 do
	local ContainerName = lib_GetWormContainerName(index)
	local lock, worm = EditContainer(ContainerName)
	
	if index < 3 then	   
		worm.SfxBankName = GM.T1_Speech
	else	
		worm.SfxBankName = GM.T2_Speech
	end
	CloseContainer(lock)
   end

    PlayIntroMovie()
    
    
	--StartFirstTurn()

end

function RenamePlayerWorm(index)
   -- Human Player: Copy "Mission Team" speech and names

   local ContainerName = lib_GetWormContainerName(index)
   local lock, worm = EditContainer(ContainerName)

   local GM = QueryContainer("GM.GameInitData")
   worm.SfxBankName = GM.T1_Speech
   if index==0 then
      worm.Name = GM.T1_W1_Name
      SetData("BriefingText.Name0", worm.Name) -- used to substitute with dialogue boxes
   elseif index==1 then
      worm.Name = GM.T1_W2_Name
      SetData("BriefingText.Name1", worm.Name)
   elseif index==2 then
      worm.Name = GM.T1_W3_Name
      SetData("BriefingText.Name2", worm.Name)
   elseif index==3 then
      worm.Name = GM.T1_W4_Name
      SetData("BriefingText.Name3", worm.Name)
   elseif index==4 then
      worm.Name = GM.T1_W5_Name
      SetData("BriefingText.Name4", worm.Name)
   elseif index==5 then
      worm.Name = GM.T1_W6_Name
      SetData("BriefingText.Name5", worm.Name)
   end

   CloseContainer(lock)
end


function SetWind()
    SetData("Wind.Speed", 0)
    
end


function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()

    if g_bFirstMoviePlayer == 0 then
        RenamePlayerWorm(0)
        g_bFirstMoviePlayer = g_bFirstMoviePlayer + 1
        StartFirstTurn()  
    elseif g_bFirstMoviePlayer == 1 then
        RenamePlayerWorm(0)
        RenamePlayerWorm(1)
        RenamePlayerWorm(2)
        StartFirstTurn()
    end

  
end
 

function Crate_Collected()

    SpawnNextCrate()   

end

function SpawnNextCrate()

    if g_bCrateStatus == 0 then
        -- crate weapon cycle 
        local Crate = {"Crate2", "Crate3", "Crate4", "Crate5", "Crate6", "Crate7", "Crate8", "Crate9"}
            lib_SpawnCrate(Crate[g_nNextCrate])
            g_nNextCrate = g_nNextCrate + 1
        
        if g_nNextCrate == 9 then 
            g_bCrateStatus = g_bCrateStatus + 1
        end
    elseif g_bCrateStatus == 1 then
        SetupDeathmatch()
        g_bCrateStatus = g_bCrateStatus + 1
    end
     
end

function SetupDeathmatch() 

   SendMessage("Weapon.Delete")
   SendMessage("Utility.Delete")
    

    SetData("TurnTime", 60000)
    SetData("SameTeamNextTurn", 1 )
    --Kill solo worm
    SendIntMessage("Worm.DieQuietly", 0)
    
    g_bDeathmatchStarted = g_bDeathmatchStarted + 1
    
    CopyContainer("DemoDMScheme", "GM.SchemeData")
    lockscheme, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 0          
    CloseContainer(lockscheme)
    
    SetData("EFMV.MovieName", "Midtro")
    SendMessage("EFMV.Play")


    EndTurn()
    
end


function TurnEnded()


    TeamCount = lib_GetActiveAlliances()
    WhichTeam = lib_GetSurvivingTeamIndex()
    local RoundTimeRemaining = GetData("RoundTimeRemaining")

--~     -- CHECK IF TIMER HAS RUN OUT
--~     if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
--~         MissionFail()
--~ 	return
--~     end
    
    -- PRE-DEATHMATCH RULES
    if g_bDeathmatchStarted == 0 then
        -- PLAYER DEAD BEFORE DEATHMATCH
        if TeamCount == 0 then
            MissionFail()
	    return
        else
            StartTurn()
        end
        
    -- POST-DEATHMATCH RULES
    else
        -- ONLY PLAYER ALIVE
        if TeamCount == 1 and WhichTeam == 0 then
            MissionSuccess()
	    return
        
        -- PLAYER DEAD DURING DEATHMATCH
        elseif TeamCount == 1 and WhichTeam == 1 then
            MissionFail()
	    return

	-- PLAYER AND CPU BOTH DEAD ON SAME TURN
	elseif TeamCount == 0 then
	    MissionFail()
	    return	    
	
        -- PLAYER AND CPU BOTH ALIVE
        else
            StartTurn()      
    
        end
    end     
    
   
end


function Timer_GameTimedOut()
    if g_bDeathmatchStarted == 0 then
        MissionFail()
    end
end


function MissionFail()

  SendMessage("GameLogic.Mission.Failure")
  lib_DisplayFailureComment()
  
end


function MissionSuccess()

   SendMessage("GameLogic.Mission.Success") 
   
   DisplaySuccessComment()
   
end     

function DoOncePerTurnFunctions()

    SendMessage("GameLogic.DropRandomCrate")
    
end

-- copied from lib help and adapted for the demo
function DisplaySuccessComment()     
   SendMessage("Commentary.Clear")
   SendMessage("Commentary.NoDefault")  
   SetData("CommentaryPanel.Comment", "Miss.Generic.Win3" )
   SetData("CommentaryPanel.Delay", 10000)
   SendMessage("CommentaryPanel.TimedText")
end

