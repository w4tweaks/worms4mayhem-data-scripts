function Initialise()
  
    SetData("TurnTime", 60000)
    
	-- Sets up mine fuse time,no duds and places them
	SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 5000)
	SetData("Mine.MaxFuse", 5000)
        SetData("Mine.DetonationType", 1) 
        
    SendMessage("GameLogic.PlaceObjects")
    
    
    --spawn 6 respawning mines with own Ids 
    SendStringMessage("GameLogic.PlaceMine", "Mine1")
    Mine1Id = GetData("Mine.Id")
    
--    SendStringMessage("GameLogic.PlaceMine", "Mine2")
--    Mine2Id = GetData("Mine.Id")
    
    SendStringMessage("GameLogic.PlaceMine", "Mine3")
    Mine3Id = GetData("Mine.Id")
    
--    SendStringMessage("GameLogic.PlaceMine", "Mine4")
--    Mine4Id = GetData("Mine.Id")
    
    SendStringMessage("GameLogic.PlaceMine", "Mine5")
    Mine5Id = GetData("Mine.Id")
    
--    SendStringMessage("GameLogic.PlaceMine", "Mine6")
--    Mine6Id = GetData("Mine.Id")
    
    SendStringMessage("GameLogic.PlaceMine", "Mine7")
    Mine7Id = GetData("Mine.Id")
    
    lib_SpawnCrate("Crate0")
    lib_SpawnCrate("Crate1")
    
	--Team and worm set up from databank
	lib_SetupTeam(0, "Team Human")
	lib_SetupWorm(0, "Player")
    lib_SetupWorm(1, "Player2")
	
	--Team and worm set up from databank
	lib_SetupTeam(1, "Team CPU")
    
	lib_SetupWorm(2, "Worm1")
	lib_SetupWorm(3, "Worm2")
	lib_SetupWorm(4, "Worm3")
	lib_SetupWorm(5, "Worm4")
    --lib_SetupWorm(6, "Worm5")
    
	--inventory called from databank
	lib_SetupTeamInventory(0, "Inventory Human")
	lib_SetupTeamInventory(1, "Inventory CPU")
	
	SendMessage("WormManager.Reinitialise")
        
        WormAILevel = "AIParams.CPU2"
        CopyContainer(WormAILevel, "AIParams.Worm02")
        CopyContainer(WormAILevel, "AIParams.Worm03") 
        CopyContainer(WormAILevel, "AIParams.Worm04")
        CopyContainer(WormAILevel, "AIParams.Worm05")
      
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 3000         
    CloseContainer(lock)
      
    g_nNextCrate = 1 
    
    BaddieDead = 0
    HumanDead = 0
    
    
	PlayIntroMovie()
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then

        StartFirstTurn()
    end
end

function Payload_Deleted()
    PayloadId = GetData("Payload.Deleted.Id")
    
    -- of any of the Id-ed mines are destroyed then respawn them
    
    if PayloadId == Mine1Id then
    
        SendStringMessage("GameLogic.PlaceMine", "Mine2")
        Mine2Id = GetData("Mine.Id")
        
    elseif PayloadId == Mine2Id then
    
        SendStringMessage("GameLogic.PlaceMine", "Mine1")
        Mine1Id = GetData("Mine.Id")
    
    elseif PayloadId == Mine3Id then
        
        SendStringMessage("GameLogic.PlaceMine", "Mine4")
        Mine4Id = GetData("Mine.Id")
    
    elseif PayloadId == Mine4Id then
    
        SendStringMessage("GameLogic.PlaceMine", "Mine3")
        Mine3Id = GetData("Mine.Id")
       
    elseif PayloadId == Mine5Id then
    
        SendStringMessage("GameLogic.PlaceMine", "Mine6")
        Mine6Id = GetData("Mine.Id")
        
    elseif PayloadId == Mine6Id then
    
        SendStringMessage("GameLogic.PlaceMine", "Mine5")
        Mine5Id = GetData("Mine.Id")
    
    elseif PayloadId == Mine7Id then
    
        SendStringMessage("GameLogic.PlaceMine", "Mine8")
        Mine8Id = GetData("Mine.Id")
        
    elseif PayloadId == Mine8Id then
    
        SendStringMessage("GameLogic.PlaceMine", "Mine7")
        Mine7Id = GetData("Mine.Id")
    
    end
end


function TurnEnded()
    
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    else
    
        StartTurn()
    end
end


function SpawnNextCrate()
    
    -- crate health cycle 
    local Crate = {"Crate0", "Crate1", "Crate3" }
        
        lib_SpawnCrate(Crate[g_nNextCrate])
        g_nNextCrate = g_nNextCrate + 1
        
    if g_nNextCrate > 3 then 
        
        g_nNextCrate = 1 
    end
    
end

function Worm_Died()

    deadworm = GetData("DeadWorm.Id")
    
    if deadworm > 1 then
    
        BaddieDead = BaddieDead + 1
        
    elseif deadworm < 2 then
    
        HumanDead = HumanDead + 1
        
    end
        
    if HumanDead == 2 then
    
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    elseif BaddieDead == 4 and HumanDead == 2 then
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    elseif BaddieDead == 4 then
    
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    
    end
end
    
--function PlayOutro()
--    
--    SetData("EFMV.MovieName", "Outro")
--    SendMessage("EFMV.Play")
--end
    