function Initialise()
	
	SetData("Land.Indestructable", 0)
	SetData("TurnTime", 0)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Human_Team")
	lib_SetupWorm(2, "WORM2")
        lib_SetupWorm(3, "WORM3")
        lib_SetupWorm(4, "WORM4")


	SendMessage("WormManager.Reinitialise")
    
        PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Outtake")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
        SendMessage("GameLogic.Mission.Success")
end
