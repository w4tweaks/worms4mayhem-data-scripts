-- Mission FastFoodDino


function Initialise()	

   SetData("TurnTime", 0)
   SetData("RoundTime", -1)

    lib_SetupTeam(0, "Player_Team")
    lib_SetupWorm(0, "Player_Worm")  

    SendMessage("WormManager.Reinitialise")

    lib_SetupTeamInventory(0, "Player_Inventory")
    
    lib_SpawnCrate("JetpackCrate")
    --lib_SpawnCrate("Crate1")
    CrateToSpawn = {"Crate1","Crate2","Crate3","Crate4","Crate5","Crate6","Crate7","Crate8","Crate9"}
    EmitterPostion = {"Party1","Party3","Party2","Party4","Party5","Party6","Party7","Party8","Party9","Party10"}
    PlayIntroMovie()
    Crate = 1
    EmitterNumber = 1
    
    WaterRate = 0
    WaterAddition = 0
    Switch = 0
    WaterRising = true
    water = 0
    ExtraFuel = 15000
    HesNotDeadReally = false
    StopWater = false
    
    SetData("Jetpack.InitFuel", 25000)
    
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
    Emitter = lib_CreateEmitter("WXP_CollectableItem", EmitterPostion[EmitterNumber]) 
end

function Crate_Collected()
    
    IndexNumber = GetData("Crate.Index")
    
    if IndexNumber == 9 then
    
        HesNotDeadReally = true
        StopWater = true
        
        lib_DeleteEmitterImmediate(Emitter)
        SetData("EFMV.GameOverMovie", "Outro")
	SetData("Water.Level", 1)
        SendMessage("GameLogic.Mission.Success")

        lib_DisplaySuccessComment()
     
    elseif IndexNumber == 10 then
            EmitterNumber = EmitterNumber + 1
            WaterRise()

            SetData("CommentaryPanel.Delay", 4000) -- 3 seconds
            SetData("CommentaryPanel.Comment", "M.Wild.Doom.NEW.2")
            SendMessage("CommentaryPanel.TimedText")
        
            lib_DeleteEmitterImmediate(Emitter)
            lib_SpawnCrate(CrateToSpawn[Crate])
            Emitter = lib_CreateEmitter("WXP_CollectableItem", EmitterPostion[EmitterNumber]) 
    else

	SendMessage("Jetpack.UpdateFuel")
        Fuel = GetData("Jetpack.Fuel")
        SetData("Jetpack.Fuel", Fuel + ExtraFuel)
        EmitterNumber = EmitterNumber + 1
        Crate = Crate + 1
        lib_DeleteEmitterImmediate(Emitter)
        lib_SpawnCrate(CrateToSpawn[Crate])
        Emitter = lib_CreateEmitter("WXP_CollectableItem", EmitterPostion[EmitterNumber]) 
        
        DisplayCollectComment()
    end
    
end


function Worm_Died()

    if HesNotDeadReally == false then
        StopWater = true
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    end
    
end

function WaterRise() -- Yes I stole this code from To boldly go but time is money people!!!

    if StopWater == false then
        WaterRiseTimer = StartTimer("rise", 250)
    end
    
end

function rise() 
        
    water = water + 1
    SetData("Water.Level", water)

            WaterRise()
end

function Crate_Destroyed()

    StopWater = true
    SendMessage("GameLogic.Mission.Failure") 
    lib_DisplayFailureComment()
end

function Crate_Sunk() -- Just in case

    StopWater = true
    SendMessage("GameLogic.Mission.Failure")
    lib_DisplayFailureComment()

end

function Timer_GameTimedOut()

    StopWater = true
    SendMessage("GameLogic.Mission.Failure")
    lib_DisplayFailureComment()
            
end


function TurnEnded()
   -- do not use the default CheckOneTeamVictory()
   StartTurn()      
end
    

function DisplayCollectComment()
   local Comment = { "C.Generic.Good.1", "C.Generic.Good.2", "C.Generic.Good.3" }
   local MyRand = lib_GetRandom(1,3)
   SendMessage("Commentary.Clear")  
   SetData("CommentaryPanel.Comment", Comment[MyRand] )
   SetData("CommentaryPanel.Delay", 3000)
   SendMessage("CommentaryPanel.TimedText")
end    
    
